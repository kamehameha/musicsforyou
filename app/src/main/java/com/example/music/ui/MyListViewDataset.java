package com.example.music.ui;

public class MyListViewDataset {

    private String imglink;
    private String name;
    private String description;

    public MyListViewDataset(String imglink, String name, String description) {
        this.imglink = imglink;
        this.name = name;
        this.description = description;
    }

    public String getImglink() {
        return imglink;
    }

    public void setImglink(String imglink) {
        this.imglink = imglink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
