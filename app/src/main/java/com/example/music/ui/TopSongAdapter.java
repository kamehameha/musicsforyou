package com.example.music.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.music.MainActivity;
import com.example.music.PlaylistSelectorActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.example.music.model.Song;
import com.example.music.ui.testfragment.threefrag;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class TopSongAdapter extends ArrayAdapter<Song> {
    private Context context;
    private int resource;
    private ArrayList<Song> arrSongList;



    public TopSongAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Song> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrSongList = objects;
    }




    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        }


//        View row = inflater.inflate(this.resource,null);

        TextView tvUser = (TextView) convertView.findViewById(R.id.tvName_topsong);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvdesc_topsong);
        TextView tvView = (TextView) convertView.findViewById(R.id.tvView_topsong);
        TextView tvrange = (TextView) convertView.findViewById(R.id.tvrange_topsong);
        CircularImageView imgAvatar = (CircularImageView) convertView.findViewById(R.id.img_topsong);


        /** Set data to row*/
        Song song = this.arrSongList.get(position);
        tvUser.setText(song.getSongname());
        tvContent.setText(song.getSinger());
//        tvView.setText(song.getLuotxem());
//        tvrange.setText(Integer.toString(threefrag.stt));
//        threefrag.stt++;
        tvrange.setText(Integer.toString(position + 1));

        tvView.setText(song.getLuotxem());
        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + song.getImg()).into(imgAvatar);

        imgAvatar.setBorderColor(Color.BLACK);
        imgAvatar.setBorderWidth(3);
        imgAvatar.setCircleColor(Color.WHITE);
        imgAvatar.setShadowRadius(3);
        imgAvatar.setShadowColor(Color.BLUE);

//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);


        return convertView;

//        return super.getView(position, convertView, parent);
    }


    @Override
    public int getCount() {
//        return super.getCount();
        return arrSongList.size();
    }

    @Nullable
    @Override
    public Song getItem(int position) {
//        return super.getItem(position);
        return arrSongList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
