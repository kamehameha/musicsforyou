package com.example.music.ui.onlineexoplayerfragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.example.music.MainActivity;
import com.example.music.OPComment;
import com.example.music.OPCommentAdapter;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.model.Song;
import com.example.music.model.User;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class playingfrag extends Fragment {

    EditText edtSearch_homefrag;
    TextView testhome;
    CircularImageView imgAvatarUserHome;
    ScrollView scrollView_onl_exo_2;

    View view;

    private ArrayList<OPComment> listcomment;
    private ListView listView_comments;
    OPCommentAdapter commentAdapter;
    Song playingSong;

    ImageView imgsong;
    TextView txtsongname;
    TextView txtcomposer;
    TextView txtsinger;
    TextView txtgenre;


    EditText myInputComment  ;
    ImageButton myInputSend ;
    RatingBar myInputRating ;


    public static PlayerView playerView;
    private PlayerControlView playerControlView;
    private SimpleExoPlayer player;

    public static SubtitleView subtitles;
    List<Cue> listCue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
         view = inflater.inflate(R.layout.frag_exo_playing, container,false);


        playerView = view.findViewById(R.id.online_player_view1);
        playerControlView=view.findViewById(R.id.online_player_control_view);

        scrollView_onl_exo_2 = view.findViewById(R.id.scrollView_onl_exo_2);

//        imgsong =  view.findViewById(R.id.imgView1);
        txtsongname =  view.findViewById(R.id.onl_exo_txtView1);
        txtcomposer =  view.findViewById(R.id.onl_exo_txtView2);
        txtsinger =  view.findViewById(R.id.onl_exo_txtView3);
        txtgenre =  view.findViewById(R.id.onl_exo_txtView4);


        listView_comments = view.findViewById(R.id.lv_online_player_comment);

        listcomment = new ArrayList<OPComment>();

        commentAdapter = new OPCommentAdapter(getContext(), R.layout.row_lv_comment_online_player, listcomment);

        listView_comments.setAdapter(commentAdapter);


        loadNewSongInfomation();

        subtitles=(SubtitleView)view.findViewById(R.id.subtitle1);

        player = MainActivity.mAudioPlayService.getPlayer();

//        subtitles.setBottom(Cue.ANCHOR_TYPE_MIDDLE);

        player.addTextOutput(new TextOutput() {
            @Override
            public void onCues(List<Cue> cues) {

                if(subtitles != null){
                    subtitles.onCues(cues);

                }
            }
        });


        playerView.getSubtitleView().setVisibility(View.GONE);


        //tat show anh mp3
//        playerView.setUseArtwork(false);

        //subtitle
//        subtitles.setStyle();
        //not use
        configureSubtitleView();

        //comment

         myInputComment = view.findViewById(R.id.myinputcomment_onl_exo_tvContent);
         myInputSend = view.findViewById(R.id.myinputcomment_onl_exo_imgbtnSend);
         myInputRating = view.findViewById(R.id.myinputcomment_onl_exo_rating);


         CircularImageView myInputAvatar = view.findViewById(R.id.myinputcomment_onl_exo_imgAvatar);
         TextView myInputUsername = view.findViewById(R.id.myinputcomment_onl_exo_tvUser);

        User user = MainActivity.USER;
        myInputUsername.setText(user.getUsername());

        myInputSend.setOnClickListener(myInputSendClickListener);

        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + user.getImg()).into(myInputAvatar);
        myInputAvatar.setBorderColor(Color.BLACK);
        myInputAvatar.setBorderWidth(3);
        myInputAvatar.setCircleColor(Color.WHITE);
        myInputAvatar.setShadowRadius(3);
        myInputAvatar.setShadowColor(Color.BLUE);
        myInputAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);

        final TextView tvViewComment =  view.findViewById(R.id.tvViewComment);
        tvViewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadCommentsOfSong();
                tvViewComment.setVisibility(View.INVISIBLE);
            }
        });


        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playingfrag_playnewsong"));

        return view;
    }

    private View.OnClickListener myInputSendClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            sendCommentUser();
        }
    };

    private void sendCommentUser() {
        String content = myInputComment.getText().toString();
        if (content.compareTo("") == 0) {

            Toast.makeText( getContext(), "Bạn hãy bình luận thêm.", Toast.LENGTH_SHORT).show();
            return;
        }

        float score = myInputRating.getRating() * 2;
        if (score == 0) {
            Toast.makeText( getContext(), "Bạn hãy chấm điểm bài hát.", Toast.LENGTH_SHORT).show();
            return;
        }

        playingSong = MainActivity.mAudioPlayService.getPlayingSong();

        if (playingSong == null) {
            Log.i("OnlineExoPlayerActivity", "sendCommentUser: playingsong is null" );
            return;
        }

        //clear comment ?

        String idsong = playingSong.getIdsong();
        if (idsong == null) {
            Log.i("OnlineExoPlayerActivity", "sendCommentUser: idsong is null" );
            return;
        }

        AndroidNetworking.post( MainActivity.HOST + "musicapi/index.php/api/playlists/sendCommentsOfSong")
                .addBodyParameter("idsong", idsong)
                .addBodyParameter("iduser", MainActivity.USER.getIduser())
                .addBodyParameter("idsubscm", "NULL") // tu tu doi sau
                .addBodyParameter("content", content)
                .addBodyParameter("rating", String.valueOf(score))  // tu tu doi sau
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");
                            if(status){
                                //reload comment chua co
                                myInputComment.setText("");

                                loadCommentsOfSong();
                                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            loadNewSongInfomation();
        }
    };

    private void loadNewSongInfomation(){
        //load info song

        loadSongInfomation();


        //load new comment
//        loadCommentsOfSong();

    }

    private void configureSubtitleView() {
        int defaultSubtitleColor = Color.argb(255, 232, 181, 36);
        int outlineColor = Color.argb(255, 43, 43, 43);



        AssetManager am = getContext().getApplicationContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(am, "fonts/opensans-regular.ttf");

        float fontScale = 2.0f;

        CaptionStyleCompat style =
                new CaptionStyleCompat(defaultSubtitleColor,
                        Color.TRANSPARENT, Color.TRANSPARENT,
                        CaptionStyleCompat.EDGE_TYPE_OUTLINE,
                        outlineColor, typeface);

        subtitles.setApplyEmbeddedStyles(false);
        subtitles.setStyle(style);
        subtitles.setFractionalTextSize(SubtitleView.DEFAULT_TEXT_SIZE_FRACTION * fontScale);

    }

    private void loadSongInfomation() {

        playingSong = MainActivity.mAudioPlayService.getPlayingSong();

        txtsongname.setText(playingSong.getSongname());
        txtcomposer.setText("Ca sĩ: " + playingSong.getComposer());
        txtsinger.setText("Sáng tác: " + playingSong.getSinger());
        txtgenre.setText("Thể loại: " + playingSong.getGenre());


        String strImage = playingSong.getImg();
        String strLinkmp3 = playingSong.getLinkmp3();

//            Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + strImage).into(imgsong);
//        StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(MainActivity.FIREBASEHOST + "musicapi/storage/" + strImage);

//        Glide.with(this /* context */)
//                .load(MainActivity.HOST + MainActivity.STORAGE + strImage)
//                .into(imgsong);

//        Toast.makeText(OnlinePlayerActivity.this, "Load " + song.getSongname(), Toast.LENGTH_SHORT).show();

//thu thap songplayed
//                                            collectSongPlayed();

    }


    private void loadCommentsOfSong() {
        playingSong = MainActivity.mAudioPlayService.getPlayingSong();

        if (playingSong == null) {
            Log.i("OnlineExoPlayerActivity", "getCommentsOfSong: playingsong not found" );
            return;
        }

        //clear comment ?

        String idsong = playingSong.getIdsong();

        if (idsong != null)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getCommentsOfSong")
                    .addBodyParameter("idsong", idsong)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String comContent;
                                    String comAt_time ;
                                    String comRating;
                                    String comUsername;
                                    String comUserImg ;
                                    String comidscm ;
                                    String comidsubscm ;
                                    String comiduser ;
                                    String comidsong ;

//                                listcomment = new ArrayList<OPComment>();

                                    listcomment.clear();

//                                    commentAdapter.notifyDataSetChanged();

                                    OPComment songcomment;
                                    HashMap<String, ArrayList<OPComment>> listSubComment =
                                            new HashMap<>();
//                                int ja_len = ja.length() > maxOfcommentforload ? maxOfcommentforload: ja.length() ;

                                    for (int i=0; i < ja.length(); i++) {
                                        comContent = ja.getJSONObject(i).getString("content");
                                        comAt_time = ja.getJSONObject(i).getString("at_time");
                                        comRating = ja.getJSONObject(i).getString("rating");
                                        comUsername = ja.getJSONObject(i).getString("username");
                                        comUserImg = ja.getJSONObject(i).getString("img");
                                        comidscm = ja.getJSONObject(i).getString("idscm");
                                        comidsubscm = ja.getJSONObject(i).getString("idsubscm");
                                        comiduser = ja.getJSONObject(i).getString("iduser");
                                        comidsong = ja.getJSONObject(i).getString("idsong");

//                                    listcomment.add(new OPComment(comUsername,comContent,comUserImg));

                                        songcomment = new OPComment(comAt_time, comRating, comidscm,
                                                comidsubscm, comiduser, comidsong, comUsername,comContent,
                                                comUserImg);

                                        if (comidsubscm.compareTo("null") == 0){
                                            listcomment.add(songcomment);
                                        }
                                        else {
                                            if (listSubComment.containsKey(comidsubscm)) {
                                                listSubComment.get(comidsubscm).add(songcomment);
                                            } else {
                                                ArrayList<OPComment> new_subcm = new ArrayList<>();
                                                new_subcm.add(songcomment);
                                                listSubComment.put(comidsubscm, new_subcm);
                                            }

                                        }
                                    }

                                    // add listSubComment to listcomment
                                    listSubCommentTolistComment(listSubComment);

                                    commentAdapter.notifyDataSetChanged();

                                    scrollView_onl_exo_2.fullScroll(ScrollView.FOCUS_UP);


//                                strImage = ja.getJSONObject(0).getString("img");
//                                strLinkmp3 = ja.getJSONObject(0).getString("linkmp3");
//                                int x = ja.length();

//                                Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + comUserImg).into(imgsong);

//                                String url = MainActivity.HOST + "musicapi/storage/general/Co-Gai-M52-HuyR-Tung-Viu.mp3"; // your URL here

//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                }
                                else {
                                    //khong tìm thấy dữ liệu comment nào
                                    listcomment.clear();

                                    commentAdapter.notifyDataSetChanged();
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }

    private void listSubCommentTolistComment(HashMap<String, ArrayList<OPComment>> listSubComment) {

        for (OPComment cm : listcomment){
            if (listSubComment.containsKey(cm.getIdscm())){
                cm.addAllSubcomments(listSubComment.get(cm.getIdscm()));
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
//        if (player == null) {
//            player = OnlineExoPlayerActivity.getPlayer();
            player = MainActivity.mAudioPlayService.getPlayer();
            playerView.setPlayer(player);
            playerControlView.setPlayer(player);
//        }

    }

    @Override
    public void onResume() {
        super.onResume();
        hideSystemUi();
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                );
//        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {


        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);

        subtitles = null;

        playerView = null;

        playerControlView.setPlayer(null);

        super.onDestroy();
    }
}
