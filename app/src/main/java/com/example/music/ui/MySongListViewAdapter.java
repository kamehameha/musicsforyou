package com.example.music.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.music.MainActivity;
import com.example.music.PlaylistSelectorActivity;
import com.example.music.R;
import com.example.music.UserActivity;
import com.example.music.model.AudioPlayService;
import com.example.music.model.PostMan;
import com.example.music.model.Song;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MySongListViewAdapter extends ArrayAdapter<Song> {
    private Context context;
    private int resource;
    private ArrayList<Song> arrSongList;
    private ListView mListView;

    private String belongtoIdpl;
    private ImageButton imgbtnReply;

    public MySongListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Song> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrSongList = objects;
    }
    public MySongListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Song> objects, ListView myList) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrSongList = objects;
        this.mListView=myList;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(resource, parent,false);
        }


//        View row = inflater.inflate(this.resource,null);

        TextView tvUser = (TextView) convertView.findViewById(R.id.tvName_mylistview);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvdesc_mylistview);

        CircularImageView imgAvatar = (CircularImageView) convertView.findViewById(R.id.img_mylistview);



        /** Set data to row*/
        Song song = this.arrSongList.get(position);
        tvUser.setText(song.getSongname());
        tvContent.setText(song.getSinger());
//        imgAvatar.setText(book.getAvatar());

        /**Set Event Onclick*/
//        btnLike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showMessage(book);
//            }
//        });

        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + song.getImg()).into(imgAvatar);

        imgAvatar.setBorderColor(Color.BLACK);
        imgAvatar.setBorderWidth(3);
        imgAvatar.setCircleColor(Color.WHITE);
        imgAvatar.setShadowRadius(3);
        imgAvatar.setShadowColor(Color.BLUE);

//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);


//        ListView lv_subcomments = convertView.findViewById(R.id.tvViewReply);

//        OPSubCommentAdapter subcommentAdapter = new OPSubCommentAdapter(convertView.getContext(),
//                R.layout.row_lv_subcomment_online_player,
//                opComment.getSubcomments());
//
//        lv_subcomments.setAdapter(subcommentAdapter);
//        subcommentAdapter.notifyDataSetChanged();


        imgbtnReply = convertView.findViewById(R.id.imgbtnPlay); // huychuong fix

        imgbtnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parentRow = (View) view.getParent();

                ListView listView = (ListView) parentRow.getParent();
                final int mposition = listView.getPositionForView(parentRow);

//                final Dialog dialog=new Dialog(view.getContext());
//                dialog.setContentView(R.layout.dialog_addnewplaylist);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.show();

//                View viewx = view.getRootView().getLagetLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog, null);

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_bottom_sheet);
                dialog.show();

                final Song song = arrSongList.get(position);

                TextView dl_songname = dialog.findViewById(R.id.tv_fragbottomsheet_songname);
                TextView dl_singer = dialog.findViewById(R.id.tv_fragbottomsheet_singer);
                LinearLayout dl_insertSong = dialog.findViewById(R.id.ln_fragbottomsheet_inserttoplaylist);
                LinearLayout dl_deleteSong = dialog.findViewById(R.id.ln_fragbottomsheet_playlistdeletesong);

                if (song != null){
                    dl_songname.setText(song.getSongname());
                    dl_singer.setText(song.getSinger());
                }


                dl_insertSong.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //hien thi danh sach playlist of user

                        Intent intent = new Intent(getContext(), PlaylistSelectorActivity.class);
//                        intent.putExtra("idsong", idsong);
//                        intent.putExtra("songname", songname);
//                        intent.putExtra("singer", singer);
//                        intent.putExtra("img", img);
                        intent.putExtra("song", (Serializable) song);

                        Toast.makeText(getContext(), "chon playlist", Toast.LENGTH_SHORT).show();

                        getContext().startActivity(intent);
                        //them mot bai vao 1 play list duoc chon

                    }
                });

                dl_deleteSong.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (belongtoIdpl == null){
                            Toast.makeText(getContext(), "Không có playlist nào được chọn.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //xoa song khoi playlist
                        String iduser = MainActivity.USER.getIduser();

                        if (iduser.compareTo("") == 0)
                            return;

                        PostMan.removeSongInsidePlaylist(iduser, belongtoIdpl, song.getIdsong(), getContext(),
                                MySongListViewAdapter.this, position);

                    }
                });

//                MainActivity.mAudioPlayService.getPlayer().setPlayWhenReady(false);
//                imgbtnReply.setImageResource(R.drawable.ic_play_circle_filled_black_30dp);
//
                AudioPlayService mService = MainActivity.mAudioPlayService;
                if(mListView!=null) {
                    if (mService.getPlayingPositionIndex() != mposition) {
//                        mService.playSongsOnline(arrSongList, mposition);
                        //mListView.setItemChecked(mposition, true);
                    } else {
                        if (mService.getPlayer().isPlaying()) {
                            //mService.getPlayer().setPlayWhenReady(false);
                            //mListView.setItemChecked(mposition, false);
                        } else {
                            //mService.getPlayer().setPlayWhenReady(true);
                            //mListView.setItemChecked(mposition, true);
                        }
                    }
                }
            }
        });
//        imgbtnReply.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Log.i("MyListViewAdapter", "imgbtnReply: onClick"  );
//            }
//        });

//        imgbtnReply.setBackgroundResource(R.drawable.gradient_yellow);


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //lam j do o day
//            }
//        });
//        if(mListView.isFocused()){
//            Toast.makeText(context,"listview focus",Toast.LENGTH_SHORT).show();
//        }

        AudioPlayService mService = MainActivity.mAudioPlayService;
        if(mListView!=null){
            if(mService.getPlayer().getPlayWhenReady() && mListView.isItemChecked(position)){ //
                    //imgbtnReply.setImageResource(R.drawable.ic_pause_black_24dp);
                   convertView.setBackgroundColor(getContext().getResources().getColor( R.color.color10));
            }
            else {
                //imgbtnReply.setImageResource(R.drawable.ic_play_circle_filled_black_30dp);
                convertView.setBackgroundColor(Color.TRANSPARENT);
            }
        }
        return convertView;

//        return super.getView(position, convertView, parent);
    }


    @Override
    public int getCount() {
//        return super.getCount();
        return arrSongList.size();
    }

    @Nullable
    @Override
    public Song getItem(int position) {
//        return super.getItem(position);
        return arrSongList.get(position);
    }

    @Override
    public int getPosition(@Nullable Song item) {
//        return super.getPosition(item);
        return arrSongList.indexOf(item);
    }

    public String getBelongtoIdpl() {
        return belongtoIdpl;
    }

    public void setBelongtoIdpl(String belongtoIdpl) {
        this.belongtoIdpl = belongtoIdpl;
    }

    public boolean removeItemWithPosition(int position) {
        if (arrSongList.size() > position) {
            arrSongList.remove(position);
            notifyDataSetChanged();
            return true;
        }
        return false;
    }

    //    @Override
//    public long getItemId(int position) {
//
//        return super.getItemId(position);
//    }
}
