package com.example.music.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.music.MainActivity;
import com.example.music.OPComment;
import com.example.music.PlaylistActivity;
import com.example.music.R;
import com.example.music.UserActivity;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



        import android.content.Context;
        import android.graphics.Color;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

        import com.mikhaellopez.circularimageview.CircularImageView;
        import com.squareup.picasso.Picasso;

        import java.util.ArrayList;
        import java.util.List;

public class MyPlayListViewAdapter extends ArrayAdapter<Playlist> {
    private Context context;
    private int resource;
    private List<Playlist> arrPlaylists;

    private String agreeRemoveIdpl="";
    private int agreeRemoveCount =0;

    public MyPlayListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Playlist> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrPlaylists = objects;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(resource, parent,false);
        }


//        View row = inflater.inflate(this.resource,null);

        TextView tvUser = (TextView) convertView.findViewById(R.id.tvName_mylistview);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvdesc_mylistview);

        CircularImageView imgAvatar = (CircularImageView) convertView.findViewById(R.id.img_mylistview);




        /** Set data to row*/
        Playlist playlist = this.arrPlaylists.get(position);
        tvUser.setText(playlist.getPlname());
        tvContent.setText(playlist.getType());
//        imgAvatar.setText(book.getAvatar());

        /**Set Event Onclick*/


        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + playlist.getImg()).into(imgAvatar);
        imgAvatar.setBorderColor(Color.BLACK);
        imgAvatar.setBorderWidth(3);
        imgAvatar.setCircleColor(Color.WHITE);
        imgAvatar.setShadowRadius(3);
        imgAvatar.setShadowColor(Color.BLUE);
//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);


//        ListView lv_subcomments = convertView.findViewById(R.id.tvViewReply);

//        OPSubCommentAdapter subcommentAdapter = new OPSubCommentAdapter(convertView.getContext(),
//                R.layout.row_lv_subcomment_online_player,
//                opComment.getSubcomments());
//
//        lv_subcomments.setAdapter(subcommentAdapter);
//        subcommentAdapter.notifyDataSetChanged();


        ImageButton imgbtnPlay = convertView.findViewById(R.id.imgbtnPlay);
//        imgbtnReply.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Log.i("MyListViewAdapter", "imgbtnReply: onClick"  );
//            }
//        });

        imgbtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Playlist pl = getItem(position);



                androidx.appcompat.widget.PopupMenu popup = new PopupMenu(getContext(),imgbtnPlay);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_option_user_playlist, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId())   {

                            case R.id.menuitem_open:

                                String idpl = pl.getIdpl();
                                Intent intent = new Intent(getContext(), PlaylistActivity.class);
                                intent.putExtra("idpl", idpl);
                                Toast.makeText(getContext(), pl.toString(), Toast.LENGTH_SHORT).show();
                                getContext().startActivity(intent);
                                break;

                            case R.id.menuitem_remove:
                                String id = pl.getIdpl();
                                if (agreeRemoveIdpl.compareTo(id) == 0){
                                    if (agreeRemoveCount >= 1) {
                                        agreeRemoveCount=0;
                                        removePlaylist(id, position);
                                        break;
                                    }
                                }

                                agreeRemoveIdpl=id;
                                agreeRemoveCount++;
                                Toast.makeText(getContext(), "Hãy thực hiện lại thao tác để chắc chắn muốn xóa playlist này!", Toast.LENGTH_SHORT).show();

                                break;

                            default:
                                break;
                        }

//                            Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();

            }
        });

//        imgbtnReply.setBackgroundResource(R.drawable.gradient_yellow);


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //lam j do o day
//            }
//        });
        return convertView;

//        return super.getView(position, convertView, parent);
    }

    public boolean removeItemWithPosition(int position) {
        if (arrPlaylists.size() > position) {
            arrPlaylists.remove(position);
            notifyDataSetChanged();
            return true;
        }
        return false;
    }

    private void removePlaylist(String idPl, int position){
        /// do it....
        PostMan.removePlaylistOfUser(MainActivity.USER.getIduser(), idPl, getContext(), this, position);
    }

    @Override
    public int getCount() {
//        return super.getCount();
        return arrPlaylists.size();
    }

    @Nullable
    @Override
    public Playlist getItem(int position) {
//        return super.getItem(position);
        return arrPlaylists.get(position);
    }

    @Override
    public int getPosition(@Nullable Playlist item) {
//        return super.getPosition(item);
        return arrPlaylists.indexOf(item);
    }



//    @Override
//    public long getItemId(int position) {
//
//        return super.getItemId(position);
//    }
}
