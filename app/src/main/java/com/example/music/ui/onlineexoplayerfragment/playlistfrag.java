package com.example.music.ui.onlineexoplayerfragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.MyListView;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.example.music.ui.MySongListViewAdapter;

import java.util.ArrayList;


public class playlistfrag extends Fragment {

    View view;

    TextView txtfragFavorites;
    private ArrayList<Song> listSong;
    private MyListView listviewSong;
    private MySongListViewAdapter songAdapter;

    private AudioPlayService mService;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
         view = inflater.inflate(R.layout.frag_exo_playlist,container,false);

        mService = MainActivity.mAudioPlayService;
        listSong = mService.getPlayingList();

//        lv_songs_playlist_adapter  = new ArrayAdapter(PlaylistActivity.this,
//                android.R.layout.simple_list_item_1, listSong);
//        lv_songs_playlist.setAdapter(lv_songs_playlist_adapter);

        listviewSong = view.findViewById(R.id.lv_playlist_exo_frag);

        songAdapter = new MySongListViewAdapter(getContext(), R.layout.row_mylistview_2, listSong, listviewSong);


        listviewSong.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listviewSong.setAdapter(songAdapter);

        //lay danh sach dang phat trong service

        listviewSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                mService.playSongWithIndex(position);

            }
        });

        int index = mService.getPlayingPositionIndex();
        listviewSong.setItemChecked(index,true);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playlistfrag_statechange"));
        return view;
    }

    public void reloadListSong(){
        listSong = mService.getPlayingList();
        songAdapter.notifyDataSetChanged();
    }

    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            //int message = intent.getIntExtra("message",-1);
//            Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            //Toast.makeText(context,String.valueOf(message),Toast.LENGTH_SHORT).show();
            String message = intent.getStringExtra("message");
            reloadListSong();
//            if(message != -1)
//                listviewSong.setItemChecked(message,true);

            AudioPlayService mService = MainActivity.mAudioPlayService;
            int position = mService.getPlayingPositionIndex();
            listviewSong.setItemChecked(position,true);
        }
    };
}
