package com.example.music.ui.testfragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.music.MainActivity;
import com.example.music.MyTopicDataset;
import com.example.music.R;
import com.example.music.SearchActivity;
import com.example.music.UserActivity;
import com.example.music.MyTopicAdapter;
import com.example.music.login;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.github.rubensousa.gravitysnaphelper.GravitySnapRecyclerView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class homefrag extends Fragment {

    EditText edtSearch_homefrag;
    TextView testhome;
    CircularImageView imgAvatarUserHome;
    View view;

    ArrayList<Playlist> myTopicDatasets;
    ArrayList<Playlist> myDataset_hottrend;
    ArrayList<Playlist> myDataset_hotchill;
    ArrayList<Playlist> myDataset_hotcomment;



    private GravitySnapRecyclerView recyclerViewTopic;
    private RecyclerView.Adapter mTopicAdapter;
    private RecyclerView.LayoutManager layoutManager;
    GravitySnapHelper snapHelper;

    private GravitySnapRecyclerView recyclerView_hottrend;
    private RecyclerView.Adapter mAdapter_hottrend;
    private RecyclerView.LayoutManager layoutManager_hottrend;
    GravitySnapHelper snapHelper_hottrend;

    private GravitySnapRecyclerView recyclerView_hotchill;
    private RecyclerView.Adapter mAdapter_hotchill;
    private RecyclerView.LayoutManager layoutManager_hotchill;
    GravitySnapHelper snapHelper_hotchill;

    private GravitySnapRecyclerView recyclerView_hotcomment;
    private RecyclerView.Adapter mAdapter_hotcomment;
    private RecyclerView.LayoutManager layoutManager_hotcomment;
    GravitySnapHelper snapHelper_hotcomment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
         view = inflater.inflate(R.layout.frag_home, container,false);

//        if (MainActivity.USER == null) {
//            MainActivity.USER = MainActivity.createUserNormal();
//        }

       // edtSearch_homefrag = view.findViewById(R.id.edtSearch_homefrag);

//        testhome = view.findViewById(R.id.tv_fraghome1);

        //imgAvatarUserHome = view.findViewById(R.id.imgAvatarUserHome);



//        Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg()).into(imgAvatarUserHome);
//
//        imgAvatarUserHome.setBorderWidth(1);
//        imgAvatarUserHome.setBorderColor(Color.RED);

        recyclerViewTopic = view.findViewById(R.id.rv_homefrag_hot_topic);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerViewTopic.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewTopic.setLayoutManager(layoutManager);

        myTopicDatasets = new ArrayList<>();
//
//        myTopicDatasets.add(new Playlist("x","safe and sound 1", "good mood"));
//        myTopicDatasets.add(new Playlist("x","safe and sound 2", "good mood"));
//        myTopicDatasets.add(new Playlist("x","safe and sound 3", "good mood"));
//        myTopicDatasets.add(new Playlist("x","safe and sound 4", "good mood"));
//        myTopicDatasets.add(new Playlist("x","safe and sound 5", "good mood"));

        snapHelper = new GravitySnapHelper(Gravity.START); //new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewTopic);


        // specify an adapter (see also next example)
        mTopicAdapter = new MyTopicAdapter(myTopicDatasets);
        recyclerViewTopic.setAdapter(mTopicAdapter);


        //hottrend
        recyclerView_hottrend =  view.findViewById(R.id.rv_homefrag_hot_trend);
        recyclerView_hottrend.setHasFixedSize(true);
        layoutManager_hottrend = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_hottrend.setLayoutManager(layoutManager_hottrend);

        myDataset_hottrend = new ArrayList<>();

//        myDataset_hottrend.add(new MyTopicDataset("x","safe and sound 11", "good mood"));
//        myDataset_hottrend.add(new MyTopicDataset("x","safe and sound 12", "good mood"));
//        myDataset_hottrend.add(new MyTopicDataset("x","safe and sound 13", "good mood"));
//        myDataset_hottrend.add(new MyTopicDataset("x","safe and sound 14", "good mood"));
//        myDataset_hottrend.add(new MyTopicDataset("x","safe and sound 15", "good mood"));


        // specify an adapter (see also next example)
        mAdapter_hottrend = new MyTopicAdapter(myDataset_hottrend);
        recyclerView_hottrend.setAdapter(mAdapter_hottrend);

        snapHelper_hottrend = new GravitySnapHelper(Gravity.START);
        snapHelper_hottrend.attachToRecyclerView(recyclerView_hottrend);


        recyclerView_hotchill =  view.findViewById(R.id.rv_homefrag_hot_chill);
        recyclerView_hotchill.setHasFixedSize(true);
        layoutManager_hotchill = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_hotchill.setLayoutManager(layoutManager_hotchill);

        myDataset_hotchill = new ArrayList<>();

        mAdapter_hotchill = new MyTopicAdapter(myDataset_hotchill);
        recyclerView_hotchill.setAdapter(mAdapter_hotchill);

        snapHelper_hotchill = new GravitySnapHelper(Gravity.START);
        snapHelper_hotchill.attachToRecyclerView(recyclerView_hotchill);



        recyclerView_hotcomment =  view.findViewById(R.id.rv_homefrag_hot_comment);
        recyclerView_hotcomment.setHasFixedSize(true);
        layoutManager_hotcomment = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_hotcomment.setLayoutManager(layoutManager_hotcomment);



        myDataset_hotcomment = new ArrayList<>();

        mAdapter_hotcomment = new MyTopicAdapter(myDataset_hotcomment);
        recyclerView_hotcomment.setAdapter(mAdapter_hotcomment);

        snapHelper_hotcomment = new GravitySnapHelper(Gravity.START);
        snapHelper_hotcomment.attachToRecyclerView(recyclerView_hotcomment);


        PostMan.getPlaylistWithType("topic", myTopicDatasets, mTopicAdapter);
        PostMan.getPlaylistWithType("hottrend", myDataset_hottrend, mAdapter_hottrend);
        PostMan.getPlaylistWithType("hotchill", myDataset_hotchill, mAdapter_hotchill);
        PostMan.getPlaylistWithType("hotcomment", myDataset_hotcomment, mAdapter_hotcomment);





//        recyclerView_hottrend.setOnI(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

//        testhome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {


//                Toast.makeText(getActivity(), "click frag home login", Toast.LENGTH_LONG).show();
//
//                Intent intent = new Intent(getContext(),login.class);
//
//                startActivityForResult(intent, 1);

                //test exoplayer


//OnlineExoPlayerActivity  | TestExoPlayerActivity
//                    Intent intent = new Intent(getContext(), OnlineExoPlayerActivity.class);
//                    startActivity(intent);


//                Intent intent = new Intent(getContext(), testFGServiceActivity.class);
//
//                startActivity(intent);


//
//            }
//        });

//        edtSearch_homefrag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent(getContext(), SearchActivity.class);
//
//                startActivity(intent);
//
//            }
//        });



        return view;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==1)
//        {
//            String message=data.getStringExtra("result");
//            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
//
//        }
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");

                Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg()).into(imgAvatarUserHome);
                Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }

        }
    }//onActivityResult
}
