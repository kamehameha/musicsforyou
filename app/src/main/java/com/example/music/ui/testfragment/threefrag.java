package com.example.music.ui.testfragment;



import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.MainActivity;
import com.example.music.R;

import com.example.music.model.Song;
import com.example.music.ui.TopSongAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class threefrag extends Fragment {

    public static int stt=1;
    private ImageView image_view;
    private ListView lvInfor;
    private ArrayList<Song> arrSong;
    private TopSongAdapter adapter;



    private View view1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.frag_three, container, false);

//        Intent i = new Intent(getContext(), threefrag.class);
//
//        startActivity(i);

        lvInfor = view1.findViewById(R.id.lvTopSong_fragthree);


        arrSong = new ArrayList<Song>();

        adapter = new TopSongAdapter(getActivity(), R.layout.row_topsong, arrSong);
        lvInfor.setAdapter(adapter);


//        view.post(new Runnable() {
//            @Override
//            public void run() {
//                // for instance
//                int height = view.getMeasuredHeight();
//                int width = view.getMeasuredWidth();
//                image_view.getLayoutParams().height = height / 22 * 4;
//                image_view.getLayoutParams().width = height / 22 * 4;
//                txtNoiDung.getLayoutParams().height = height / 22;
//                lvInfor.getLayoutParams().height = height / 44 * 25;
//            }
//        });


        XuLy();

        getTopSong();

        return view1;
    }


    private void XuLy() {
        lvInfor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Text", "Onclick để chuyển màn hình");//dùng để chuyển sang nghe các bài nhạc thuộc playlist
                Song song = (Song) parent.getItemAtPosition(position);

                String idsong = song.getIdsong();

                ArrayList<Song> listsong = new ArrayList<>();
                listsong.add(song);
                MainActivity.mAudioPlayService.playSongsOnline(listsong, 0);

                //Toast.makeText(getContext(), song.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        lvInfor.setOnItemLongClickListener(new AdapterView
                .OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {

                return false;
            }
        });

        //image_view.setImageResource(R.drawable.ic_launcher_foreground);
    }


    private void getTopSong() {

            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/songs/getTopSongWeek")
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String idpl;
                                    String idsong;
                                    String songname;
                                    String composer;
                                    String singer;
                                    String genre;
                                    String img;
                                    String linkmp3;
                                    String rating_;
                                    String created_at;
                                    String iduser;
                                    String lyric;
                                    String solannghe;

                                    Song song;

                                    for (int i = 0; i < ja.length(); i++) {

                                        idsong = ja.getJSONObject(i).getString("idsong");
                                        songname = ja.getJSONObject(i).getString("songname");
                                        composer = ja.getJSONObject(i).getString("composer");
                                        singer = ja.getJSONObject(i).getString("singer");
                                        genre = ja.getJSONObject(i).getString("genre");
                                        img = ja.getJSONObject(i).getString("img");
                                        linkmp3 = ja.getJSONObject(i).getString("linkmp3");
                                        rating_ = ja.getJSONObject(i).getString("rating_");
                                        created_at = ja.getJSONObject(i).getString("created_at");
                                        iduser = ja.getJSONObject(i).getString("iduser");
                                        lyric = ja.getJSONObject(i).getString("lyric");
                                        solannghe = ja.getJSONObject(i).getString("solannghe");


                                        song = new Song(idsong, songname,
                                                composer, singer, genre, img, linkmp3,
                                                rating_, created_at, iduser, lyric, solannghe);

                                        arrSong.add(song);
//                                        arrstt.add(stt);
//                                        stt++;
                                    }
                                    adapter.notifyDataSetChanged();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });

    }


}
