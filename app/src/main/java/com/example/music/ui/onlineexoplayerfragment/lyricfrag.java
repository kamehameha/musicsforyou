package com.example.music.ui.onlineexoplayerfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.MainActivity;
import com.example.music.MyListView;
import com.example.music.R;
import com.example.music.SeaAllFragment;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.example.music.model.Song;
import com.example.music.ui.MyPlayListViewAdapter;
import com.example.music.ui.MySongListViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class lyricfrag extends Fragment {

    private View view;
    ArrayList<Song> listSong;

    MyListView listviewSong;

    MySongListViewAdapter songAdapter;
    ArrayList<Playlist> listPlaylist;

    MyListView listviewPlaylist ;

    MyPlayListViewAdapter playlistAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_exo_lyric,container,false);

        listSong = new ArrayList<>();




        listviewSong = view.findViewById(R.id.lv_lyricfrag_songs);

        songAdapter = new MySongListViewAdapter(getContext(), R.layout.row_mylistview_2, listSong);

        listviewSong.setAdapter(songAdapter);



        listPlaylist = new ArrayList<>();



        listviewPlaylist = view.findViewById(R.id.lv_lyricfrag_playlists);

        playlistAdapter = new MyPlayListViewAdapter(getContext(), R.layout.row_mylistview_2, listPlaylist);

        listviewPlaylist.setAdapter(playlistAdapter);

//        implementSuggestAll();

        PostMan.getSongRecommend(MainActivity.USER.getIduser(), listSong, songAdapter);

        PostMan.getPlaylistRecommend(MainActivity.USER.getIduser(), listPlaylist, playlistAdapter);


        listviewSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Song song = listSong.get(position);

                ArrayList<Song> playsong = new ArrayList<>();
                playsong.add(song);

                MainActivity.mAudioPlayService.playSongsOnline(playsong,0);

            }
        });

        listviewPlaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ArrayList<Song> playlist = new ArrayList<>();
                String idpl = listPlaylist.get(position).getIdpl();

                PostMan.getSongsOfPlaylist(idpl, playlist, null, true, 0);

            }
        });

        return view;
    }

    private void implementSuggestAll() {
        String key = "i";

//        if (edt_searchactiv_value.getText().toString().compareTo("") == 0)
//            return;

        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/searchAllSource")
                .addBodyParameter("value", key)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONObject jo = response.getJSONObject("response");

                                String idpl;
                                String idsong ;
                                String songname;
                                String composer;
                                String singer ;
                                String genre ;
                                String img ;
                                String linkmp3 ;
                                String rating_ ;
                                String created_at ;
                                String iduser ;
                                String lyric ;

                                Song song;

                                listSong.clear();

                                JSONArray jsong = jo.getJSONArray("songs");

                                for (int i=0; i < jsong.length(); i++) {

                                    idsong = jsong.getJSONObject(i).getString("idsong");
                                    songname = jsong.getJSONObject(i).getString("songname");
                                    composer = jsong.getJSONObject(i).getString("composer");
                                    singer = jsong.getJSONObject(i).getString("singer");
                                    genre = jsong.getJSONObject(i).getString("genre");
                                    img = jsong.getJSONObject(i).getString("img");
                                    linkmp3 = jsong.getJSONObject(i).getString("linkmp3");
                                    rating_ = jsong.getJSONObject(i).getString("rating_");
                                    created_at = jsong.getJSONObject(i).getString("created_at");
                                    iduser = jsong.getJSONObject(i).getString("iduser");
                                    lyric = jsong.getJSONObject(i).getString("lyric");


                                    song = new Song( idsong, songname,
                                            composer, singer, genre, img,linkmp3,
                                            rating_, created_at, iduser, lyric);

                                    listSong.add(song);

                                }

//                                String idpl;
                                String plname ;
//                                String img;
                                String type;
//                                String rating_ ;
                                String plstatus ;
                                String shared ;
//                                String iduser ;
//                                String created_at ;

                                Playlist pl;

                                listPlaylist.clear();

                                JSONArray jplaylist = jo.getJSONArray("playlists");

                                for (int i=0; i < jplaylist.length(); i++) {
                                    idpl = jplaylist.getJSONObject(i).getString("idpl");
                                    plname = jplaylist.getJSONObject(i).getString("plname");
                                    img = jplaylist.getJSONObject(i).getString("img");
                                    type = jplaylist.getJSONObject(i).getString("type");
                                    rating_ = jplaylist.getJSONObject(i).getString("rating_");
                                    plstatus = jplaylist.getJSONObject(i).getString("status");
                                    shared = jplaylist.getJSONObject(i).getString("shared");
                                    iduser = jplaylist.getJSONObject(i).getString("iduser");
                                    created_at = jplaylist.getJSONObject(i).getString("created_at");


                                    pl = new Playlist(idpl, plname, img,
                                            type, rating_, plstatus, shared,iduser,
                                            created_at);

                                    listPlaylist.add(pl);

                                }


                                //goi frag hien thi
//                                songAdapter.notifyDataSetChanged();

                                songAdapter.notifyDataSetChanged();
                                playlistAdapter.notifyDataSetChanged();
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                //test player manager
//                                SystemMusic.addSongsToPlaylingList(listSong);
//                                SystemMusic.playSongsList(listSong, 0);
                            }
                            else
                            {
                                Toast.makeText(getContext(), "Không tìm thấy kết quả nào" , Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

}
