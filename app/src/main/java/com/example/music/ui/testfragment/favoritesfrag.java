package com.example.music.ui.testfragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.music.OfflinePlayerActivity;
import com.example.music.R;


public class favoritesfrag extends Fragment {

    View view;

    TextView txtfragFavorites;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
//         view =inflater.inflate(R.layout.frag_favorites,container,false);
        view =inflater.inflate(R.layout.frag_favorites,container,false);



        txtfragFavorites = view.findViewById(R.id.txtfragFavorites);
        Intent offlineAudioIntent = new Intent(getContext(), OfflinePlayerActivity.class);
        startActivity(offlineAudioIntent);
        txtfragFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent audioManagerIntent = new Intent(getContext(), AudioManager.class);
////                audioManagerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                audioManagerIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                startActivity(audioManagerIntent);

            }
        });


        return view;
    }

}
