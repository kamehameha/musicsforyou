package com.example.music;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.Song;
import com.example.music.model.User;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SubCommentActivity extends AppCompatActivity {

    EditText myInputComment  ;
    ImageButton myInputSend ;
    RatingBar myInputRating ;

    ScrollView myScrollView;

    String idscm;
    String idsong;

    private ArrayList<OPComment> listcomment;
    private ListView listView_comments;
    OPSubCommentAdapter commentAdapter;


    private ArrayList<OPComment> list_main_comment;
    private ListView listView_main_comments;
    OPSubCommentAdapter main_commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_comment);



        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                idscm= null;
                idsong= null;
            } else {
                idscm= extras.getString("idscm");
                idsong= extras.getString("idsong");
            }
        } else {
            idscm= (String) savedInstanceState.getSerializable("idscm");
            idsong= (String) savedInstanceState.getSerializable("idsong");
        }

        if (idscm == null || idsong == null) {
            Toast.makeText(getApplicationContext(), "Đầu vào không có giá trị ID SongComment", Toast.LENGTH_SHORT).show();
        }

        myInputComment = findViewById(R.id.activ_sub_comment_onl_exo_tvContent);
        myInputSend = findViewById(R.id.activ_sub_comment_onl_exo_imgbtnSend);
        myInputRating = findViewById(R.id.activ_sub_comment_onl_exo_rating);

        myScrollView = findViewById(R.id.sv_activ_sub_comment);

// main comment
        listView_main_comments = findViewById(R.id.mlv_activ_main_comment);

        list_main_comment = new ArrayList<OPComment>();

        main_commentAdapter = new OPSubCommentAdapter(this, R.layout.row_mlv_subcomment_online_player, list_main_comment);

        listView_main_comments.setAdapter(main_commentAdapter);

// subcomment
        listView_comments = findViewById(R.id.mlv_activ_sub_comment_1);

        listcomment = new ArrayList<OPComment>();

        commentAdapter = new OPSubCommentAdapter(this, R.layout.row_mlv_subcomment_online_player, listcomment);

        listView_comments.setAdapter(commentAdapter);

        CircularImageView myInputAvatar = findViewById(R.id.activ_sub_comment_onl_exo_imgAvatar);
        TextView myInputUsername = findViewById(R.id.activ_sub_comment_onl_exo_tvUser);

        User user = MainActivity.USER;
        myInputUsername.setText(user.getUsername());

        myInputSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCommentUser();

            }
        });

        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + user.getImg()).into(myInputAvatar);
        myInputAvatar.setBorderColor(Color.BLACK);
        myInputAvatar.setBorderWidth(3);
        myInputAvatar.setCircleColor(Color.WHITE);
        myInputAvatar.setShadowRadius(3);
        myInputAvatar.setShadowColor(Color.BLUE);
        myInputAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);


        //load subcomment of idsongcomment

        loadSubComments();
//        commentAdapter.notifyDataSetChanged();
    }


    private void loadSubComments() {


        if (idscm == null) {
            Log.i("OnlineExoPlayerActivity", "loadSubComments: IDComments not found" );
            return;
        }



        if (idscm != null)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/comments/getsubcomment")
                    .addBodyParameter("idscm", idscm)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String comContent;
                                    String comAt_time ;
                                    String comRating;
                                    String comUsername;
                                    String comUserImg ;
                                    String comidscm ;
                                    String comidsubscm ;
                                    String comiduser ;
                                    String comidsong ;

//                                listcomment = new ArrayList<OPComment>();

                                    listcomment.clear();
                                    list_main_comment.clear();

//                                    commentAdapter.notifyDataSetChanged();

                                    OPComment songcomment;
                                    HashMap<String, ArrayList<OPComment>> listSubComment =
                                            new HashMap<>();
//                                int ja_len = ja.length() > maxOfcommentforload ? maxOfcommentforload: ja.length() ;

                                    for (int i=0; i < ja.length(); i++) {
                                        comContent = ja.getJSONObject(i).getString("content");
                                        comAt_time = ja.getJSONObject(i).getString("at_time");
                                        comRating = ja.getJSONObject(i).getString("rating");
                                        comUsername = ja.getJSONObject(i).getString("username");
                                        comUserImg = ja.getJSONObject(i).getString("img");
                                        comidscm = ja.getJSONObject(i).getString("idscm");
                                        comidsubscm = ja.getJSONObject(i).getString("idsubscm");
                                        comiduser = ja.getJSONObject(i).getString("iduser");
                                        comidsong = ja.getJSONObject(i).getString("idsong");

//                                    listcomment.add(new OPComment(comUsername,comContent,comUserImg));

                                        songcomment = new OPComment(comAt_time, comRating, comidscm,
                                                comidsubscm, comiduser, comidsong, comUsername,comContent,
                                                comUserImg);

                                        if (comidsubscm.compareTo("null") == 0){
                                            //comment parent
                                            list_main_comment.add(songcomment);

                                        }
                                        else {
                                            //sub comment
                                            listcomment.add(songcomment);

//                                            if (listSubComment.containsKey(comidsubscm)) {
//                                                listSubComment.get(comidsubscm).add(songcomment);
//                                            } else {
//                                                ArrayList<OPComment> new_subcm = new ArrayList<>();
//                                                new_subcm.add(songcomment);
//                                                listSubComment.put(comidsubscm, new_subcm);
//                                            }
//
                                        }
                                    }

                                    // add listSubComment to listcomment


                                    commentAdapter.notifyDataSetChanged();
                                    main_commentAdapter.notifyDataSetChanged();

                                    myScrollView.fullScroll(ScrollView.FOCUS_UP);


//                                strImage = ja.getJSONObject(0).getString("img");
//                                strLinkmp3 = ja.getJSONObject(0).getString("linkmp3");
//                                int x = ja.length();

//                                Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + comUserImg).into(imgsong);

//                                String url = MainActivity.HOST + "musicapi/storage/general/Co-Gai-M52-HuyR-Tung-Viu.mp3"; // your URL here

//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                }
                                else {
                                    //khong tìm thấy dữ liệu comment nào
                                    listcomment.clear();

                                    commentAdapter.notifyDataSetChanged();
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }

    private void sendCommentUser() {
        String content = myInputComment.getText().toString();
        if (content.compareTo("") == 0) {

            Toast.makeText( this, "Bạn hãy bình luận thêm.", Toast.LENGTH_SHORT).show();
            return;
        }

        float score = myInputRating.getRating() * 2;
        if (score == 0) {
            Toast.makeText( this, "Bạn hãy chấm điểm bài hát.", Toast.LENGTH_SHORT).show();
            return;
        }



        //clear comment ?

        if (idscm == null) {
            Log.i("OnlineExoPlayerActivity", "sendCommentUser: IDComments not found" );
            return;
        }

        AndroidNetworking.post( MainActivity.HOST + "musicapi/index.php/api/playlists/sendCommentsOfSong")
                .addBodyParameter("idsong", idsong)
                .addBodyParameter("iduser", MainActivity.USER.getIduser())
                .addBodyParameter("idsubscm", idscm)
                .addBodyParameter("content", content)
                .addBodyParameter("rating", String.valueOf(score))  // tu tu doi sau
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");
                            if(status){
                                //reload comment chua co
                                myInputComment.setText("");

                                loadSubComments();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}
