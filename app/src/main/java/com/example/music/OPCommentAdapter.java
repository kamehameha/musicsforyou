package com.example.music;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OPCommentAdapter extends ArrayAdapter<OPComment> {
    private Context context;
    private int resource;
    private List<OPComment> arrOPComment;

    public OPCommentAdapter(@NonNull Context context, int resource, @NonNull ArrayList<OPComment> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrOPComment = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        }


//        View row = inflater.inflate(this.resource,null);

        TextView tvUser = (TextView) convertView.findViewById(R.id.tvUser);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);


        CircularImageView imgAvatar = (CircularImageView) convertView.findViewById(R.id.imgAvatar);



        /** Set data to row*/
        final OPComment opComment = this.arrOPComment.get(position);
        tvUser.setText(opComment.getUser());
        tvContent.setText(opComment.getContent());

//        imgAvatar.setText(book.getAvatar());

        /**Set Event Onclick*/
//        btnLike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showMessage(book);
//            }
//        });

        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + opComment.getAvatar()).into(imgAvatar);
        imgAvatar.setBorderColor(Color.BLACK);
        imgAvatar.setBorderWidth(3);
        imgAvatar.setCircleColor(Color.WHITE);
        imgAvatar.setShadowRadius(3);
        imgAvatar.setShadowColor(Color.BLUE);
//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);

//        ListView lv_subcomments = convertView.findViewById(R.id.tvViewReply);

//        OPSubCommentAdapter subcommentAdapter = new OPSubCommentAdapter(convertView.getContext(),
//                R.layout.row_lv_subcomment_online_player,
//                opComment.getSubcomments());
//
//        lv_subcomments.setAdapter(subcommentAdapter);
//        subcommentAdapter.notifyDataSetChanged();

        String time = opComment.getAt_time();
        TextView tvTime = convertView.findViewById(R.id.tvCommentAtTime);
        tvTime.setText(time);

        TextView tvCommentReply = convertView.findViewById(R.id.tvViewReply);


        int numOfComment = opComment.getSubcomments().size();
        if (numOfComment > 0) {
            String strNumOfComment = Integer.toString(numOfComment);
            tvCommentReply.setText("XEM " + strNumOfComment + " TRẢ LỜI");

            tvCommentReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //click xem cac tra loi -> show form cac tra loi
                    Log.i("OnlineExoPlayerActivity", "tvCommentReply: onClick" );
                }
            });
        }

        ImageButton imgbtnReply = convertView.findViewById(R.id.imgbtnReply1);
        final View finalConvertView = convertView;
        imgbtnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OnlineExoPlayerActivity", "imgbtnReply: onClick" );
                OPComment currentComm = getItem(position);

                //active sub comment

                Intent i = new Intent(v.getContext(), SubCommentActivity.class);
                i.putExtra("idscm", currentComm.getIdscm());
                i.putExtra("idsong", currentComm.getIdsong());


                v.getContext().startActivity(i);

//                startActivity(i);
            }
        });

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //lam j do o day
//            }
//        });
        return convertView;

//        return super.getView(position, convertView, parent);
    }
}
