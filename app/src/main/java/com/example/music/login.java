package com.example.music;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.User;
import com.example.music.offline.MyUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class login extends AppCompatActivity {

    TextInputLayout username;
    TextInputLayout password;
    Button login;
//    Button register;
    TextView register;
    Button intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        username = findViewById(R.id.edtUsernameLogin);
        password =   findViewById(R.id.edtPasswordLogin);
        login = (Button) findViewById(R.id.button2);
        register = findViewById(R.id.sign_up);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginRun();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerRun();
            }
        });
    }
    private void registerRun() {
//        if (username == null || password == null)
//            return;
//        if (username.getEditText().getText() == null || password.getEditText().getText() == null)
//            return;
//
        if(confirmInputRes()){
        String usn = username.getEditText().getText().toString();
        String usp = password.getEditText().getText().toString();

        AndroidNetworking.post( MainActivity.HOST + "musicapi/index.php/api/users/register")
                .addBodyParameter("username", usn)
                .addBodyParameter("password",  usp)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");
                            if(status){
//                                JSONArray ja = response.getJSONArray("response");
//                                Log.d("username", ja.getJSONObject(0).getString("username"));
                                showMessage(message);

                            }
                            else {
                                username.setError("Tài khoản đã tồn tại");
                                password.setError(null);
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });}
    }

    private void loginRun() {
        //test
//        if (false){
//            Intent i = new Intent(login.this, com.example.music.OnlinePlayerActivity.class);
//            startActivity(i);
//            return;
//        }
//        if (username == null || password == null)
//            return;
//        if (username.getEditText().getText() == null || password.getEditText().getText() == null)
//            return;
        if(confirmInputLog()){
        String usn = username.getEditText().getText().toString();
        String usp = password.getEditText().getText().toString();


        AndroidNetworking.post( MainActivity.HOST + "musicapi/index.php/api/users/login")
                .addBodyParameter("username", usn)
                .addBodyParameter("password", usp)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");
                            if(status){
                                JSONArray ja = response.getJSONArray("response");
                                String iduser = ja.getJSONObject(0).getString("iduser");
                                String username = ja.getJSONObject(0).getString("username");
                                String imglink = ja.getJSONObject(0).getString("img");
                                String account_type = ja.getJSONObject(0).getString("account_type");
                                String created_at = ja.getJSONObject(0).getString("created_at");

                                MainActivity.USER = new User(iduser, username, imglink, account_type, created_at);
//                                Log.d("username", ja.getJSONObject(0).getString("username"));

                                MyUtil myutil = new MyUtil(getApplicationContext());
                                myutil.storeUser(MainActivity.USER);


                                showMessage(message);

                                Intent returnIntent = getIntent();
                                returnIntent.putExtra("result", "Login OK"); //login OK
//                                returnIntent.putExtra("result", "Login OK");
                                setResult(RESULT_OK, returnIntent);
                                finish();}
                            else{
                                password.setError("Tài khoản hoặc mật khẩu không đúng");
                                username.setError(" ");



//                                Intent i = new Intent(login.this, com.example.music.OnlinePlayerActivity.class);
//                                startActivity(i);
                            }


                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });}
    }

    private void showMessage(String message) {
        Toast.makeText(login.this, message, Toast.LENGTH_LONG).show();
    }
    private boolean validateUserName(){
        String usernameInput=this.username.getEditText().getText().toString().trim();
        if(usernameInput.isEmpty()){
            username.setError("Không thể bỏ trống");
            return false;
        }else {
            username.setError(null);
            return true;
        }
    }
    private boolean validatePassWordRes(){
        String passwordInput=this.password.getEditText().getText().toString().trim();
        if(passwordInput.isEmpty()){
            password.setError("Không thể bỏ trống");
            return false;
        }else if(passwordInput.length()<=3){
            password.setError("Tối thiểu 3 kí tự");
            return false;
        }
        else {
            username.setError(null);
            return true;
        }
    }
    private boolean validatePassWordLog(){
        String passwordInput=this.password.getEditText().getText().toString().trim();
        if(passwordInput.isEmpty()){
            password.setError("Không thể bỏ trống");
            return false;
        }
        else {
            username.setError(null);
            return true;
        }
    }
    private boolean confirmInputRes(){
        if(!validatePassWordRes()|!validateUserName())
            return false;
        else return true;
    }
    private boolean confirmInputLog(){
        if(!validatePassWordLog()|!validateUserName())
            return false;
        else return true;
    }


}
