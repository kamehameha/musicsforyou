package com.example.music.model;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;

import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.offline.MyUtil;
import com.example.music.offline.OfflinePlayingActivity;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;

import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.Format;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AudioPlayService extends Service { //implements AudioManager.OnAudioFocusChangeListener
    private static final String CHANNEL_ID = "This is the channel";
    private static final int NOTIFICATION_ID = 101;

    private boolean isPlayingOnline = true;

    private static final String TAG = "AUDIOPLAYSERVICE_TAG";
    private Context context;
    private SimpleExoPlayer player;
    private PlayerNotificationManager playerNotificationManager;
    private MediaSessionConnector mediaSessionConnector;
    private MediaSessionCompat mediaSession;
    private AudioManager audioManager;
    private BroadcastReceiver audioBecomingNoisy;
    private IntentFilter audioBecomingNoisyIntentFilter;
    private MyListener myListener;


    MediaSource[] mediaSources; //
    ConcatenatingMediaSource concatenatingMediaSource; //thong tin windowindex dang phat
    int position;

    IBinder mBinder = new LocalBinder();
    public class LocalBinder extends Binder {
        public AudioPlayService getServerInstance() {
            return AudioPlayService.this;
        }
    }

    public String getTime() {
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mDateFormat.format(new Date());
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public AudioPlayService() {
        context = this;
    }


    @Override
    public void onCreate() {
        super.onCreate();
//        player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
//
//        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "MyExoPlayer"));
//
//        ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
//                .createMediaSource(Uri.parse(getString(R.string.media_url_mp3)));



//        playerView = findViewById(R.id.video_view);

        player = ExoPlayerFactory.newSimpleInstance(this);

//        playerView.setPlayer(player);

//        Uri uri = Uri.parse(getString(R.string.media_url_mp3));
//        MediaSource mediaSource = buildMediaSource(uri);


//        player.prepare(mediaSource);
//        player.setPlayWhenReady(false);

        concatenatingMediaSource = new ConcatenatingMediaSource();


        myListener = new MyListener();
        player.addListener(myListener);
        //refer to https://developer.android.com/guide/topics/media-apps/audio-focus
        //audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioBecomingNoisy = new AudioBecomingNoisy();
        audioBecomingNoisyIntentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);

        playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(context, CHANNEL_ID,
                R.string.playBack_channelName,
                R.string.playBack_channelDescription, NOTIFICATION_ID,
                new PlayerNotificationManager.MediaDescriptionAdapter() {
                    @Override
                    public String getCurrentContentTitle(Player player) {
                        return  playingList!=null?playingList.get(player.getCurrentWindowIndex()).getSongname():"Good Mood";
                    }

                    @Nullable
                    @Override
                    public PendingIntent createCurrentContentIntent(Player player) {
//                        boolean isPlayingOnline = getPlayingSong().getLinkmp3().startsWith("http://192.168.0.4:8080/");
                        Intent intent;
                        if(isPlayingOnline)
                           intent  = new Intent(context, OnlineExoPlayerActivity.class);
                        else
                            intent = new Intent(getApplicationContext(), OfflinePlayingActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    @Nullable
                    @Override
                    public String getCurrentContentText(Player player) {
                        return playingList!=null?playingList.get(player.getCurrentWindowIndex()).getSinger():null;
                    }

                    Bitmap bm = null;
                    @Nullable
                    @Override
                    public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback callback) {
                        //HuyChuong them cho nay

                        if(playingList!=null) {
                            if(isPlayingOnline)
                                Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + getPlayingSong().getImg()).into(new Target() {
                                    @Override
                                    public void onBitmapLoaded( Bitmap bitmap, Picasso.LoadedFrom from) {
                                        bm = bitmap;
                                    }

                                    @Override
                                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                                    }
                                });
                            else
                                bm = new MyUtil(context).getAlbumArt(playingList.get((player.getCurrentWindowIndex())).getLinkmp3());

//                            if(bm==null)
//                                bm = BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
                        }
                        if(bm==null)
                            bm = BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
                        return bm;
                    }
                },
                new PlayerNotificationManager.NotificationListener() {
                    @Override
                    public void onNotificationCancelled(int notificationId, boolean dismissedByUser) {
                        stopForeground(false);
                        stopSelf();
                    }

                    @Override
                    public void onNotificationPosted(int notificationId, Notification notification, boolean ongoing) {
                        startForeground(notificationId, notification);
                    }
                }


        );



//        player.addTextOutput(new TextOutput() {
//            @Override
//            public void onCues(List<Cue> cues) {
//
//            }
//        });


        playerNotificationManager.setPlayer(player);
        mediaSession = new MediaSessionCompat(context, TAG);
        mediaSession.setActive(true);
        playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());
        mediaSessionConnector = new MediaSessionConnector(mediaSession);
        mediaSessionConnector.setPlayer(player);




        audioManager=(AudioManager) this.getSystemService(this.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

    }

    private boolean isPlayerPauseByLossFocus = false;
    AudioManager.OnAudioFocusChangeListener afChangeListener =  new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
//                    // Permanent loss of audio focus
//                    // Pause playback immediate
                player.setPlayWhenReady(false);
                //isPlayerPauseByLossFocus=true;
//                    // Wait 30 seconds before stopping playback
//                    handler.postDelayed(delayedStopRunnable,
//                            TimeUnit.SECONDS.toMillis(30));
            }
            else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                // Pause playback
                if(player.getPlayWhenReady())
                    isPlayerPauseByLossFocus=true;
                player.setPlayWhenReady(false);

            }
            else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                // Lower the volume, keep playing
//                    final float volume = (float) (1 - (Math.log(Offline - 18) / Math.log(maxVolume)));
//                    mediaPlayer.setVolume(volume, volume);
                //isPlayerPauseByLossFocus=true;
            }
            else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // Your app has been granted audio focus again
                // Raise volume to normal, restart playback if necessary
                if(isPlayerPauseByLossFocus){
                    player.setPlayWhenReady(true);
                }
                isPlayerPauseByLossFocus=false;

            }
        }
    };


    private MediaSource buildMediaSource(ArrayList<Uri> uris, ArrayList<Uri> uris_lyric) { //Uri... uris
        // These factories are used to construct two media sources below
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, "exoplayer-codelab");
        ProgressiveMediaSource.Factory mediaSourceFactory =
                new ProgressiveMediaSource.Factory(dataSourceFactory);


        // For subtitles

//        String srt_link = getString(R.string.media_url_subtitle_test1);
        //"https://www.iandevlin.com/html5test/webvtt/upc-video-subtitles-en.vtt";
        //getString(R.string.media_url_subtitle_test);

//        if((srt_link==null|| srt_link=="false" || srt_link.isEmpty())){
//
//            showAd=true;
//            return mediaSource;
//        }

        Format textFormat = Format.createTextSampleFormat(null, MimeTypes.TEXT_VTT,
                Format.NO_VALUE,"en");
        //Log.e("srt link is",srt_link);

//        Format subtitleFormat = Format.createTextSampleFormat(
//                null, // An identifier for the track. May be null.
//                MimeTypes.TEXT_VTT, // The mime type. Must be set correctly.
//                0, // Selection flags for the track.
//                null); // The subtitle language. May be null.

//        Uri uriSubtitle = Uri.parse(srt_link);

        MediaSource subtitleSource;

        MergingMediaSource mergedSource;

        MediaSource[] mediaSources = new MediaSource[uris.size()];
        MediaSource mediaSource;
        for (int i=0; i < uris.size(); i++) {

            mediaSource = mediaSourceFactory.createMediaSource(uris.get(i));

//             subtitleSource = new SingleSampleMediaSource(uriSubtitle, dataSourceFactory, textFormat, C.TIME_UNSET);
            subtitleSource =
                    new SingleSampleMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(uris_lyric.get(i), textFormat, C.TIME_UNSET);

            mergedSource = new MergingMediaSource(mediaSource, subtitleSource);

            mediaSources[i] = mergedSource; //mediaSource;
        }



        concatenatingMediaSource = new ConcatenatingMediaSource(mediaSources);


        return concatenatingMediaSource;
//        return mediaSources.length == 1 ? mediaSources[0]: concatenatingMediaSource;
    }

    private MediaSource buildMediaSource_Offline(ArrayList<Uri> uris, ArrayList<Uri> uris_lyric) { //Uri... uris
        // These factories are used to construct two media sources below
        // ArrayList<Uri> uris_lyric Trường hợp Offline đưa vào null được không ?
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, "exoplayer-codelab");
        ProgressiveMediaSource.Factory mediaSourceFactory =
                new ProgressiveMediaSource.Factory(dataSourceFactory);


        // For subtitles

//        String srt_link = getString(R.string.media_url_subtitle_test1);
        //"https://www.iandevlin.com/html5test/webvtt/upc-video-subtitles-en.vtt";
        //getString(R.string.media_url_subtitle_test);

//        if((srt_link==null|| srt_link=="false" || srt_link.isEmpty())){
//
//            showAd=true;
//            return mediaSource;
//        }

        Format textFormat = Format.createTextSampleFormat(null, MimeTypes.TEXT_VTT,
                Format.NO_VALUE,"en");
        //Log.e("srt link is",srt_link);

//        Format subtitleFormat = Format.createTextSampleFormat(
//                null, // An identifier for the track. May be null.
//                MimeTypes.TEXT_VTT, // The mime type. Must be set correctly.
//                0, // Selection flags for the track.
//                null); // The subtitle language. May be null.

//        Uri uriSubtitle = Uri.parse(srt_link);

        MediaSource subtitleSource;

        MergingMediaSource mergedSource;

        MediaSource[] mediaSources = new MediaSource[uris.size()];
        MediaSource mediaSource;
        if(uris_lyric==null) { // huychuong fix cho nay
            for (int i = 0; i < uris.size(); i++) {

                mediaSource = mediaSourceFactory.createMediaSource(uris.get(i));

                mediaSources[i] = mediaSource; //mediaSource;
            }


        }
        concatenatingMediaSource = new ConcatenatingMediaSource(mediaSources);


        return concatenatingMediaSource;
//        return mediaSources.length == 1 ? mediaSources[0]: concatenatingMediaSource;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(player!=null)
            MyUtil.storePosition((int)player.getCurrentPosition());

        player.removeListener(myListener);
        mediaSession.release();
        mediaSessionConnector.setPlayer(null);
        playerNotificationManager.setPlayer(null);
        player.release();
        player = null;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return  START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return mBinder;

    }


//    @Override
//    public void onAudioFocusChange(int focusChange) {
//        String stateString;
//        switch (focusChange) {
//            case AudioManager.AUDIOFOCUS_GAIN:
//                player.setPlayWhenReady(true);
//                break;
//            case AudioManager.AUDIOFOCUS_LOSS:
//                player.setPlayWhenReady(false);
//                break;
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//                player.setPlayWhenReady(false);
//                break;
//                //tăng giảm âm lượng tức thới
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
//                break;
//        }
//    }

    private class AudioBecomingNoisy extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (player.getPlayWhenReady()){
                player.setPlayWhenReady(false);
            }
        }
    }
    private boolean isSendMessageToPlayingFrag = false;
    //refer to https://codelabs.developers.google.com/codelabs/exoplayer-intro/#5
    // https://exoplayer.dev/doc/reference/com/google/android/exoplayer2/Player.EventListener.html
    private class MyListener implements Player.EventListener {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//            int requestAudioFocus = audioManager.requestAudioFocus(AudioPlayService.this,
     //               AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

//https://developer.android.com/reference/android/media/AudioFocusRequest
//            AudioAttributes mPlaybackAttributes = new AudioAttributes.Builder()
//                    .setUsage(AudioAttributes.USAGE_MEDIA)
//                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
//                    .build();
//            AudioFocusRequest mFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
//                    .setAudioAttributes(mPlaybackAttributes)
//                    .setAcceptsDelayedFocusGain(true)
//                    .setWillPauseWhenDucked(true)
//                    .setOnAudioFocusChangeListener(this)
//                    .build();

//            int requestAudioFocus = audioManager.requestAudioFocus(AudioPlayService.this,
//                    AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);


            if (playbackState == Player.STATE_READY && playWhenReady) {
                //Player is playing
                //request AudioFocus here.
               // AudioPlayService.this.registerReceiver(audioBecomingNoisy, audioBecomingNoisyIntentFilter);

                //sendMessage_StateChange_ToPlaylistFrag("play");

//                if (requestAudioFocus == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
//                    player.setPlayWhenReady(true);
//                }else{
//                    player.setPlayWhenReady(false);
//                }

            } else if (playbackState == Player.STATE_READY ) {

                //Player paused.
                //Abandon audio focus here.
                //AudioPlayService.this.unregisterReceiver(audioBecomingNoisy);
                //if(playWhenReady==false)
                //sendMessage_StateChange_ToPlaylistFrag("pause");

                //audioManager.abandonAudioFocus(AudioPlayService.this);

//                audioManager.abandonAudioFocusRequest();

            }
            if(playbackState == PlaybackStateCompat.STATE_PAUSED){

                Log.i(TAG, "Pause clicked");

            }
            if(isPlayingOnline)
                sendMessage_StateChange_ToPlaylistFrag("pause");

        }

        @Override
        public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

        }


        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            if(!isSendMessageToPlayingFrag){
                sendMessageToPlayingFrag();
                isSendMessageToPlayingFrag=true;
            } else  isSendMessageToPlayingFrag=false;

            MyUtil.storeAudio(getPlayingSong());


            // co the dung cho next song previous
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {

        }

        @Override
        public void onPlaybackSuppressionReasonChanged(int playbackSuppressionReason) {

        }

        @Override
        public void onIsPlayingChanged(boolean isPlaying) {

        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {

            Log.i(TAG, "ERROR: " + error.getMessage());
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
//THIS METHOD GETS CALLED FOR EVERY NEW SOURCE THAT IS PLAYED
            int latestWindowIndex = player.getCurrentWindowIndex();

            if (latestWindowIndex != position) {
                // item selected in playlist has changed, handle here
                position = latestWindowIndex;
                // ...
                //thu thap bai hat duoc played
                collectSongPlayed();

            }
            MyUtil.storePosition((int)player.getCurrentPosition());
            Log.i("storeposition",String.valueOf( player.getCurrentPosition()));


        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {

        }
    }

    private void collectSongPlayed() {
        String idsong = playingList.get(position).getIdsong();
        String iduser = MainActivity.USER.getIduser();
        PostMan.collectSongPlayed(idsong, iduser);

    }

    private ArrayList<Song> playingList;

    public void playSongsOnline(ArrayList<Song> playingList, int position) {

        ArrayList<Uri> uris = new ArrayList<>();
//        ArrayList<Uri> uris_before = new ArrayList<>();
//        ArrayList<Uri> uris_after = new ArrayList<>();

        ArrayList<Uri> uris_lyric = new ArrayList<>();
        Song song;
        String url;
        String url_lyric;
        for (int i =0 ; i < playingList.size() ; i++) {
            song = playingList.get(i);
            url = MainActivity.HOST + MainActivity.STORAGE + song.getLinkmp3();
            url_lyric= MainActivity.HOST + MainActivity.STORAGE + song.getLyric();

            uris.add(Uri.parse(url));
            uris_lyric.add(Uri.parse(url_lyric));
//            if (i < position)
//                uris_after.add(Uri.parse(url));
//            else
//                uris_before.add(Uri.parse(url));

        }

//        uris.addAll(uris_after);
//
//        uris.addAll(uris_before);


        MediaSource mediaSource = buildMediaSource(uris,uris_lyric);

//        if (concatenatingMediaSource.getSize() >= 1 && position >= 2)
//            mediaSource = concatenatingMediaSource.getMediaSource(position);
        this.playingList = playingList;
        this.isPlayingOnline = true;

        player.prepare(mediaSource);
        player.seekTo(position, C.TIME_UNSET);
        player.setPlayWhenReady(true);

        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.setShuffleModeEnabled(true);

        this.position = position; //not use

        //thu thap bai hat duoc played
        collectSongPlayed();

//        player.getCurrentWindowIndex();
    }

    // huychuong them vao cho nay
    public void playSongsOffline(ArrayList<Song> playingList, int position){
        ArrayList<Uri> uris = new ArrayList<>();
        if(playingList == this.playingList){
            playSongWithIndex(position);
            return;
        }


        for(Song song : playingList){
            uris.add(Uri.parse(song.getLinkmp3()));
        }
        MediaSource mediaSource = buildMediaSource_Offline(uris,null);

//        if (concatenatingMediaSource.getSize() >= 1 && position >= 2)
//            mediaSource = concatenatingMediaSource.getMediaSource(position);

        this.playingList = playingList;

        player.prepare(mediaSource);
        player.seekTo(position, C.TIME_UNSET);
        player.setPlayWhenReady(true);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.setShuffleModeEnabled(true);


        this.position = position; //not use
        this.isPlayingOnline = false;
//        player.getCurrentWindowIndex();
    }

    public ArrayList<Song> getPlayingList() {
        return playingList;
    }

    public int getPlayingPositionIndex() {
        return player.getCurrentWindowIndex();
    }

    public Song getPlayingSong(){
        int index = getPlayingPositionIndex();
        return playingList.get(index);
    }

    public boolean isPlayingOnline(){return this.isPlayingOnline;}

    public void playSongWithIndex(int position){
        if (position >= 0 && position < playingList.size()) {
            player.seekTo(position, C.TIME_UNSET);
            player.setPlayWhenReady(true);
        }
    }

    // Send an Intent with an action named "custom-event-name". The Intent sent should
// be received by the ReceiverActivity.
    private void sendMessageToPlayingFrag() {
        Log.d("sendMessageToPlayingFrag_sender", "Broadcasting message");
        Intent intent = new Intent("playingfrag_playnewsong");
        // You can also include some extra data.
        intent.putExtra("message", "This is my message! - " + position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }


    private void sendMessageToPlaylistFrag() {
//        Log.d("sendMessageToPlayingFrag_sender", "Broadcasting message");
        Intent intent = new Intent("playlistfrag_playnewsong");
        // You can also include some extra data.
        intent.putExtra("message", position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendMessage_StateChange_ToPlaylistFrag(String playWhenReady){
        Intent intent = new Intent("playlistfrag_statechange");
        // You can also include some extra data.
        intent.putExtra("message",playWhenReady);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
