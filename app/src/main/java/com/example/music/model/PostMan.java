package com.example.music.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.MainActivity;
import com.example.music.MyTopicAdapter;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.ui.MyPlayListViewAdapter;
import com.example.music.ui.MySongListViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PostMan {



    public static void getSongsOfPlaylist(String idpl, final ArrayList<Song> listSong, final ArrayAdapter<Song> songAdapter, final boolean playWhenResponse, final int position
                ) {
        if (idpl != null)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getSongsOfPlaylist")
                    .addBodyParameter("idpl", idpl) // idpl muon show
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String idpl;
                                    String idsong ;
                                    String songname;
                                    String composer;
                                    String singer ;
                                    String genre ;
                                    String img ;
                                    String linkmp3 ;
                                    String rating_ ;
                                    String created_at ;
                                    String iduser ;
                                    String lyric ;

                                    Song song;

                                    for (int i=0; i < ja.length(); i++) {
                                        idpl = ja.getJSONObject(i).getString("idpl");
                                        idsong = ja.getJSONObject(i).getString("idsong");
                                        songname = ja.getJSONObject(i).getString("songname");
                                        composer = ja.getJSONObject(i).getString("composer");
                                        singer = ja.getJSONObject(i).getString("singer");
                                        genre = ja.getJSONObject(i).getString("genre");
                                        img = ja.getJSONObject(i).getString("img");
                                        linkmp3 = ja.getJSONObject(i).getString("linkmp3");
                                        rating_ = ja.getJSONObject(i).getString("rating_");
                                        created_at = ja.getJSONObject(i).getString("created_at");
                                        iduser = ja.getJSONObject(i).getString("iduser");
                                        lyric = ja.getJSONObject(i).getString("lyric");


                                        song = new Song( idsong, songname,
                                                composer, singer, genre, img,linkmp3,
                                                rating_, created_at, iduser, lyric);

                                        listSong.add(song);

                                    }
                                    if (songAdapter != null)
                                        songAdapter.notifyDataSetChanged();

                                    if (playWhenResponse) {
                                        int cposition = position >= listSong.size() ? 0: position;
                                        MainActivity.mAudioPlayService.playSongsOnline(listSong, cposition);


                                    }
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                    //test player manager
//                                SystemMusic.addSongsToPlaylingList(listSong);
//                                SystemMusic.playSongsList(listSong, 0);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }



    public static void removeSongInsidePlaylist(String iduser, String idpl, String idsong, final Context context, final MySongListViewAdapter songAdapter, final int position) {
        if (iduser != null && idpl != null && idsong != null )
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/removeSongInsidePlaylist")
                    .addBodyParameter("iduser", iduser)
                    .addBodyParameter("idpl", idpl)
                    .addBodyParameter("idsong", idsong)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {

                                    songAdapter.removeItemWithPosition(position);

                                    if (context != null)
                                        Toast.makeText( context, message, Toast.LENGTH_SHORT).show();



                                }
                                else {

                                    if (context != null)
                                        Toast.makeText( context, message, Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }



    public static void getPlaylistWithType(String type, final ArrayList<Playlist> listPlaylist, final RecyclerView.Adapter playlistAdapter) {
        if (type != null && type.compareTo("playlist") != 0)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getPlaylistWithType")
                    .addBodyParameter("type", type) // idpl muon show
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String idpl;
                                    String plname ;
                                    String img;
                                    String type;
                                    String rating_ ;
                                    String plstatus ;
                                    String shared ;
                                    String iduser ;
                                    String created_at ;

                                    Playlist pl;

                                    for (int i=0; i < ja.length(); i++) {
                                        idpl = ja.getJSONObject(i).getString("idpl");
                                        plname = ja.getJSONObject(i).getString("plname");
                                        img = ja.getJSONObject(i).getString("img");
                                        type = ja.getJSONObject(i).getString("type");
                                        rating_ = ja.getJSONObject(i).getString("rating_");
                                        plstatus = ja.getJSONObject(i).getString("status");
                                        shared = ja.getJSONObject(i).getString("shared");
                                        iduser = ja.getJSONObject(i).getString("iduser");
                                        created_at = ja.getJSONObject(i).getString("created_at");


                                        pl = new Playlist(idpl, plname, img,
                                                type, rating_, plstatus, shared,iduser,
                                                created_at);

                                        listPlaylist.add(pl);

                                    }
                                    if (playlistAdapter != null)
                                        playlistAdapter.notifyDataSetChanged();
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }


    public static void collectSongPlayed(String idsong, String iduser) {
        if (idsong != null) {
//            String iduser = "NULL";

            if (iduser == null)
                iduser = "1";


            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/collectSongsplayed")
                    .addBodyParameter("idsong", idsong)
                    .addBodyParameter("iduser", iduser)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");
                                Log.d("collectSongsplayed", message);

                                if (status) {


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
        }
    }


    public static void getSongRecommend(String iduser, final ArrayList<Song> listSong, final ArrayAdapter<Song> songAdapter
    ) {
        if (iduser != null)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/songs/getSongRecommend")
                    .addBodyParameter("iduser", iduser)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");

                                    String idpl;
                                    String idsong ;
                                    String songname;
                                    String composer;
                                    String singer ;
                                    String genre ;
                                    String img ;
                                    String linkmp3 ;
                                    String rating_ ;
                                    String created_at ;
                                    String iduser ;
                                    String lyric ;

                                    Song song;
                                    listSong.clear();

                                    for (int i=0; i < ja.length(); i++) {
//                                        idpl = ja.getJSONObject(i).getString("idpl");
                                        idsong = ja.getJSONObject(i).getString("idsong");
                                        songname = ja.getJSONObject(i).getString("songname");
                                        composer = ja.getJSONObject(i).getString("composer");
                                        singer = ja.getJSONObject(i).getString("singer");
                                        genre = ja.getJSONObject(i).getString("genre");
                                        img = ja.getJSONObject(i).getString("img");
                                        linkmp3 = ja.getJSONObject(i).getString("linkmp3");
                                        rating_ = ja.getJSONObject(i).getString("rating_");
                                        created_at = ja.getJSONObject(i).getString("created_at");
                                        iduser = ja.getJSONObject(i).getString("iduser");
                                        lyric = ja.getJSONObject(i).getString("lyric");


                                        song = new Song( idsong, songname,
                                                composer, singer, genre, img,linkmp3,
                                                rating_, created_at, iduser, lyric);

                                        listSong.add(song);

                                    }

                                    if (songAdapter != null)
                                        songAdapter.notifyDataSetChanged();


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }


    public static void getPlaylistRecommend(String iduser, final ArrayList<Playlist> listPlaylist, final ArrayAdapter<Playlist> playlistAdapter) {
        if (iduser == null)
            return;

        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getPlaylistRecommend")
                .addBodyParameter("iduser", iduser)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONArray ja = response.getJSONArray("response");

                                String idpl;
                                String plname ;
                                String img;
                                String type;
                                String rating_ ;
                                String plstatus ;
                                String shared ;
                                String iduser ;
                                String created_at ;
                                String solanplaylist ;

                                Playlist pl;

                                listPlaylist.clear();

                                for (int i=0; i < ja.length(); i++) {
                                    idpl = ja.getJSONObject(i).getString("idpl");
                                    plname = ja.getJSONObject(i).getString("plname");
                                    img = ja.getJSONObject(i).getString("img");
                                    type = ja.getJSONObject(i).getString("type");
                                    rating_ = ja.getJSONObject(i).getString("rating_");
                                    plstatus = ja.getJSONObject(i).getString("status");
                                    shared = ja.getJSONObject(i).getString("shared");
                                    iduser = ja.getJSONObject(i).getString("iduser");
                                    created_at = ja.getJSONObject(i).getString("created_at");
                                    solanplaylist = ja.getJSONObject(i).getString("solanplaylist"); // chua sai`



                                    pl = new Playlist(idpl, plname, img,
                                            type, rating_, plstatus, shared,iduser,
                                            created_at);

                                    listPlaylist.add(pl);

                                }

                                if (playlistAdapter != null)
                                    playlistAdapter.notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }


    public static void removePlaylistOfUser(String iduser, String idpl, final Context context, final MyPlayListViewAdapter playlistAdapter, final int position) {
        if (iduser != null && idpl != null )
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/removePlaylistOfUser")
                    .addBodyParameter("iduser", iduser)
                    .addBodyParameter("idpl", idpl)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {

                                    playlistAdapter.removeItemWithPosition(position);

                                    if (context != null)
                                        Toast.makeText( context, message, Toast.LENGTH_SHORT).show();



                                }
                                else {

                                    if (context != null)
                                        Toast.makeText( context, message, Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
    }



}
