package com.example.music.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Song implements Serializable {

    private String idsong;
    private String songname;
    private String composer;
    private String singer;
    private String genre;
    private String img;
    private String linkmp3;
    private String rating_;
    private String created_at;
    private String iduser;
    private String lyric;
    private String album = ""; // huychuong thêm vô cho thằng offline


    private String luotxem;
    private long duration=0;
    private String albumId;

    public Song(String idsong, String songname, String composer, String singer, String genre,
                String img, String linkmp3, String rating_, String created_at, String iduser,String lyric) {
        this.idsong = idsong;
        this.songname = songname; // Title
        this.composer = composer; //
        this.singer = singer; //Artist
        this.genre = genre;
        this.img = img; //Album Art
        this.linkmp3 = linkmp3; // Data
        this.rating_ = rating_;
        this.created_at = created_at;
        this.iduser = iduser;
        this.lyric = lyric;
    }

    public Song(String linkmp3, String songname, String album, String singer, long duration , String albumid//Offline
    ) {
        this.idsong = "offline";
        this.songname = songname; // Title
        this.composer = ""; //
        this.singer = singer; //Artist
        this.genre = "";
        this.img = ""; //Album Art
        this.linkmp3 = linkmp3; // Data
        this.rating_ = "";
        this.created_at = "";
        this.iduser = "";
        this.lyric = "";
        this.album = album;
        this.duration=duration;
        this.albumId = albumid;
    }
    public Song(String idsong, String songname, String composer, String singer, String genre,//Lễ top song
                String img, String linkmp3, String rating_, String created_at, String iduser,String lyric,String luotxem) {
        this.idsong = idsong;
        this.songname = songname; // Title
        this.composer = composer; //
        this.singer = singer; //Artist
        this.genre = genre;
        this.img = img; //Album Art
        this.linkmp3 = linkmp3; // Data
        this.rating_ = rating_;
        this.created_at = created_at;
        this.iduser = iduser;
        this.lyric = lyric;
        this.luotxem=luotxem;
    }
    public String getAlbumId(){return this.albumId;}

    public String getIdsong() {
        return idsong;
    }

    public void setIdsong(String idsong) {
        this.idsong = idsong;
    }

    public String getSongname() {
        return songname;
    }

    public void setSongname(String songname) {
        this.songname = songname;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLinkmp3() {
        return linkmp3;
    }

    public void setLinkmp3(String linkmp3) {
        this.linkmp3 = linkmp3;
    }

    public String getRating_() {
        return rating_;
    }

    public void setRating_(String rating_) {
        this.rating_ = rating_;
    }

    public String getCreated_at() {
        return created_at;
    }

    public long getDuration(){return this.duration;}

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getAlbum() {
        return album;
    }
    public String getLuotxem() {
        return luotxem;
    }

    public void setLuotxem(String luotxem) {
        this.luotxem = luotxem;
    }
    @NonNull
    @Override
    public String toString() {
        return songname + " - " + composer + " - " + singer;
    }
}
