package com.example.music.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Playlist {



    private String idpl;
    private String plname;
    private String img;
    private String type;
    private String rating_;
    private String status;
    private String shared;
    private String iduser;
    private String created_at;



    private ArrayList<Song> songlist;
    private int currentSong;

    public Playlist(String idpl, String plname, String img, String type, String rating_, String status,
                    String shared, String iduser, String created_at) {
        this.idpl = idpl;
        this.plname = plname;
        this.img = img;
        this.type = type;
        this.rating_ = rating_;
        this.status = status;
        this.shared = shared;
        this.iduser = iduser;
        this.created_at = created_at;
        this.songlist = null;
    }

    public int getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(int currentSong) {
        this.currentSong = currentSong;
    }

    public String getIdpl() {
        return idpl;
    }

    public void setIdpl(String idpl) {
        this.idpl = idpl;
    }

    public String getPlname() {
        return plname;
    }

    public void setPlname(String plname) {
        this.plname = plname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRating_() {
        return rating_;
    }

    public void setRating_(String rating_) {
        this.rating_ = rating_;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShared() {
        return shared;
    }

    public void setShared(String shared) {
        this.shared = shared;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ArrayList<Song> getSonglist() {
        return songlist;
    }

    public void setSonglist(ArrayList<Song> songlist) {
        this.songlist = songlist;
    }

    @NonNull
    @Override
    public String toString() {
        return plname + " - " + idpl;
    }
}
