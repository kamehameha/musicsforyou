package com.example.music.model;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    private String iduser;
    private String username;
    private String img;
    private String account_type;
    private String created_at;
    private ArrayList<Playlist> myplaylist;

    public User(String iduser, String username, String imglink, String account_type, String created_at) {
        this.iduser = iduser;
        this.username = username;
        this.img = imglink;
        this.account_type = account_type;
        this.created_at = created_at;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String imglink) {
        this.img = imglink;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ArrayList<Playlist> getMyplaylist() {
        return myplaylist;
    }

    public void setMyplaylist(ArrayList<Playlist> myplaylist) {
        this.myplaylist = myplaylist;
    }
}
