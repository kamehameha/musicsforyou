package com.example.music;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.example.music.ui.onlineexoplayerfragment.lyricfrag;
import com.example.music.ui.onlineexoplayerfragment.playingfrag;
import com.example.music.ui.onlineexoplayerfragment.playlistfrag;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OnlineExoPlayerActivity extends AppCompatActivity {

    private PlayerView playerView;
    private static SimpleExoPlayer player;

    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;

//    private PlaybackStateListener playbackStateListener;
    private static final String TAG = OnlineExoPlayerActivity.class.getName();

    public static SimpleExoPlayer getPlayer() {
        return player;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_exoplayer);


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navi_online_exoplayer);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);


//        bottomNavigationView.setSelectedItemId(R.id.nav_online_exo_2);
//
//
        getSupportFragmentManager().beginTransaction().replace(R.id.containerfrag_online_exo,
                new playlistfrag()).commit();
//        View viewbv = bottomNavigationView.findViewById(R.id.nav_online_exo_2);
//        viewbv.performClick();

//        Menu menu = bottomNavigationView.getMenu();
//        navListener.onNavigationItemSelected(menu.findItem(R.id.nav_online_exo_2));


    }



    private void initializePlayer() {
        if (player == null ) {
            player = MainActivity.mAudioPlayService.getPlayer();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT >= 24) {
            initializePlayer();
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        if ((Util.SDK_INT < 24 || player == null)) {
            initializePlayer();
        }
    }



    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT < 24) {
        releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
    }



    private void releasePlayer() {
        if (player != null) {
//            playWhenReady = player.getPlayWhenReady();
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();

//            player.removeListener(playbackStateListener);

//            player.release();
            player = null;
        }
    }



    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_online_exo_1:
                            selectedFragment = new playlistfrag();
                            break;
                        case R.id.nav_online_exo_2:
                            selectedFragment = new playingfrag();
                            break;
                        case R.id.nav_online_exo_3:
                            selectedFragment = new lyricfrag();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.containerfrag_online_exo,
                            selectedFragment).commit();

                    return true;
                }
            };


}
