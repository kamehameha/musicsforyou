package com.example.music;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.example.music.model.Song;

import java.util.ArrayList;

public class MyTopicAdapter extends RecyclerView.Adapter<MyTopicAdapter.MyViewHolder> {

//    private String[] mDataset;
    private ArrayList<Playlist> mTopicDatasets;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public TextView textViewdesc;
        public ImageView imgTopic;
        public View view;

        public MyViewHolder(View v) {
            super(v);
            view = v;
            textView = (TextView) v.findViewById(R.id.tv_topic_itemname);
            textViewdesc = v.findViewById(R.id.tv_topic_itemdesc);

            imgTopic = v.findViewById(R.id.im_topic_image);
//            imgTopic.setBackgroundResource(R.drawable.layout_bg_image);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyTopicAdapter(ArrayList<Playlist> mTopicDatasets) {
//        mDataset = myDataset;
        this.mTopicDatasets = mTopicDatasets;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyTopicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_topic_items, parent, false);


        final MyViewHolder vh = new MyViewHolder(v);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = vh.getLayoutPosition();

                Playlist playlist = mTopicDatasets.get(position);

                //choi 1 playlist
                PostMan.getSongsOfPlaylist(playlist.getIdpl(), new ArrayList<Song>(), null,true,0);
            }
        });



        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.textView.setText(mDataset[position]);


        String plname = mTopicDatasets.get(position).getPlname();
        holder.textView.setText(plname);

        String rating = mTopicDatasets.get(position).getRating_();

        holder.textViewdesc.setText("Rating: " + rating);

        String linkimg = mTopicDatasets.get(position).getImg();
        Glide.with(holder.view.getContext())
                .load(MainActivity.HOST + MainActivity.STORAGE + linkimg)
                .into(holder.imgTopic);




    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mTopicDatasets.size();
    }
}
