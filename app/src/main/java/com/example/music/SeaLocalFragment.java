package com.example.music;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;

import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.example.music.offline.CustomGridAlbumAdapter;
import com.example.music.offline.CustomListSongAdapter;
import com.example.music.offline.MyUtil;
import com.example.music.offline.OfflineFragTabAlbum;
import com.example.music.offline.OfflineFragTabPlaylist;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SeaLocalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SeaLocalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeaLocalFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public static SeaLocalFragment instance ;
    public SeaLocalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SeaLocalFragment.
     */


    // TODO: Rename and change types and number of parameters
    public static SeaLocalFragment newInstance(String param1, String param2) {
        SeaLocalFragment fragment = new SeaLocalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        instance=fragment;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private ListView lvSearchSong;
    private GridView gvSearchAlbum;

    private ArrayList<Song>  listSearchSong;
    private ArrayList<Song> listSearchAlbum;

    private AudioPlayService mService;

    CustomListSongAdapter songAdapter;
    CustomGridAlbumAdapter albumAdapter;


    public CustomListSongAdapter getSongAdapter() {
        return songAdapter;
    }

    public CustomGridAlbumAdapter getAlbumAdapter(){
        return albumAdapter;
    }

    private ArrayList<Song> getListSongByAlbum(String albumName){
        ArrayList<Song> resultList = new ArrayList<>();

        //albumName = MyUtil.removeAccent(albumName);
        String album;
        for(Song song: OfflineFragTabPlaylist.mListSong){
            album = song.getAlbum();
            if(album.equals(albumName))
                resultList.add(song);
        }

        return resultList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sea_local, container, false);

        mService=MainActivity.mAudioPlayService;

        listSearchSong=new ArrayList<>();
        listSearchAlbum=new ArrayList<>();

        lvSearchSong = view.findViewById(R.id.offline_lv_BaiHatTimDuoc);
        gvSearchAlbum = view.findViewById(R.id.offline_gv_AlbumTimDuoc);

        songAdapter = new CustomListSongAdapter(getContext(),listSearchSong);
        lvSearchSong.setAdapter(songAdapter);
        lvSearchSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if(listSearchSong!=null && listSearchSong.size()>0) {
//                        mService.playSongsOffline(listSearchSong, i);
//                }

                //view.setBackgroundColor(Color.BLACK);
                ArrayList<Song> playsong = new ArrayList<>();
                playsong.add(listSearchSong.get(i));
                mService.playSongsOffline(playsong,0);

            }
        });

        albumAdapter=new CustomGridAlbumAdapter(getContext(),listSearchAlbum);
        gvSearchAlbum.setAdapter(albumAdapter);
        gvSearchAlbum.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Song song = (Song)gvSearchAlbum.getItemAtPosition(i);
                ArrayList<Song> listSongOfAlbum = getListSongByAlbum(song.getAlbum());
                mService.playSongsOffline(listSongOfAlbum,0);
            }
        });

        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String str) {
        if (mListener != null) {
            mListener.onSearchLocalFragmentInteraction(str);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSearchLocalFragmentInteraction(String str);
    }

    public void renewListSong(ArrayList<Song> listSong) {

        this.listSearchSong.clear();
        this.listSearchSong.addAll(listSong);

    }

    public void renewListAlbum(ArrayList<Song> listAlbum){
        this.listSearchAlbum.clear();
        this.listSearchAlbum.addAll(listAlbum);
    }
}
