package com.example.music;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.Song;
import com.example.music.ui.MySongListViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlaylistActivity extends AppCompatActivity {
    private ListView lv_songs_playlist;
    private ArrayList<Song> listSong;
    ArrayAdapter lv_songs_playlist_adapter;
    String idpl;

    MySongListViewAdapter songAdapter;
    MyListView listviewSong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);



        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                idpl= null;
            } else {
                idpl= extras.getString("idpl");
            }
        } else {
            idpl= (String) savedInstanceState.getSerializable("idpl");
        }


        if (idpl == null){
            Toast.makeText(getApplicationContext(), "Đầu vào không có giá trị ID Playlist", Toast.LENGTH_SHORT).show();
        }

        TextView test = findViewById(R.id.tv_playlistactivity);

//        lv_songs_playlist  = findViewById(R.id.lv_songs_playlistactiv);

        listSong = new ArrayList<>();


//        lv_songs_playlist_adapter  = new ArrayAdapter(PlaylistActivity.this,
//                android.R.layout.simple_list_item_1, listSong);
//        lv_songs_playlist.setAdapter(lv_songs_playlist_adapter);

        listviewSong = findViewById(R.id.lv_songs_playlistactiv);

        songAdapter = new MySongListViewAdapter(this, R.layout.row_mylistview_2, listSong,listviewSong);

        listviewSong.setAdapter(songAdapter);

        songAdapter.setBelongtoIdpl(idpl);

        getSongsOfPlaylist();
        //thu thap playlist view
        collectPlaylistviewed();

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        listviewSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Song song = (Song) parent.getItemAtPosition(position);

                String idsong = song.getIdsong();



//                SystemMusic.playSongsList(listSong, position);

                Toast.makeText(getApplicationContext(), song.toString(), Toast.LENGTH_SHORT).show();

//                Intent intent = new Intent(getApplicationContext(), OnlinePlayerActivity.class);
//                intent.putExtra("idsong", idsong);
//
//                startActivity(intent);

                MainActivity.mAudioPlayService.playSongsOnline(listSong, position);
                Intent intent = new Intent(getApplicationContext(), OnlineExoPlayerActivity.class);
                startActivity(intent);

            }
        });
    }

    private void getSongsOfPlaylist() {
        if (idpl != null)
            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getSongsOfPlaylist")
                .addBodyParameter("idpl", idpl) // idpl muon show
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONArray ja = response.getJSONArray("response");

                                String idpl;
                                String idsong ;
                                String songname;
                                String composer;
                                String singer ;
                                String genre ;
                                String img ;
                                String linkmp3 ;
                                String rating_ ;
                                String created_at ;
                                String iduser ;
                                String lyric ;

                                Song song;

                                for (int i=0; i < ja.length(); i++) {
                                    idpl = ja.getJSONObject(i).getString("idpl");
                                    idsong = ja.getJSONObject(i).getString("idsong");
                                    songname = ja.getJSONObject(i).getString("songname");
                                    composer = ja.getJSONObject(i).getString("composer");
                                    singer = ja.getJSONObject(i).getString("singer");
                                    genre = ja.getJSONObject(i).getString("genre");
                                    img = ja.getJSONObject(i).getString("img");
                                    linkmp3 = ja.getJSONObject(i).getString("linkmp3");
                                    rating_ = ja.getJSONObject(i).getString("rating_");
                                    created_at = ja.getJSONObject(i).getString("created_at");
                                    iduser = ja.getJSONObject(i).getString("iduser");
                                    lyric = ja.getJSONObject(i).getString("lyric");


                                    song = new Song( idsong, songname,
                                            composer, singer, genre, img,linkmp3,
                                            rating_, created_at, iduser, lyric);

                                    listSong.add(song);

                                }
                                songAdapter.notifyDataSetChanged();
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                //test player manager
//                                SystemMusic.addSongsToPlaylingList(listSong);
//                                SystemMusic.playSongsList(listSong, 0);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }


    private void collectPlaylistviewed() {
        if (idpl != null) {
            String iduser = "NULL";
            if (MainActivity.USER != null)
                iduser = MainActivity.USER.getIduser();


            AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/collectPlaylistviewed")
                    .addBodyParameter("idpl", idpl)
                    .addBodyParameter("iduser", iduser)
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            try {
                                Boolean status = response.getBoolean("status");
                                String message = response.getString("message");

                                if (status) {
                                    JSONArray ja = response.getJSONArray("response");


                                    Toast.makeText(getApplicationContext(), "played playlist " + idpl, Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
        }
    }
}
