package com.example.music;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.example.music.ui.onlineexoplayerfragment.lyricfrag;
import com.example.music.ui.onlineexoplayerfragment.playingfrag;
import com.example.music.ui.onlineexoplayerfragment.playlistfrag;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OfflinePlayerActivity extends AppCompatActivity {

    private static SimpleExoPlayer player;
    private AudioPlayService mService;

    private static final int REQUEST_CODE_READ_EXTERNAL_PERMISSION = 2;
    //    private PlaybackStateListener playbackStateListener;
    private static ArrayList<Song> myListAudio;
    public static SimpleExoPlayer getPlayer() {
        return player;
    }

    private ArrayList<Song> loadAudio() {
        ContentResolver contentResolver = getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);
        //audioList = new ArrayList<>();
        //MyUtil myUtil = new MyUtil(getApplicationContext());
        //boolean isFirstSaveAlbumImages = myUtil.loadStateSaveAlbumImages();
        ArrayList<Song> audioList = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            //MyUtil myutil = new MyUtil(getApplicationContext());

            int index = 0;
            while (cursor.moveToNext()) {

                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String albumId = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                int duration = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                // Save to audioList
                audioList.add(new Song(data, title, album, artist,duration,albumId));
            }

        }
        cursor.close();
        return audioList;
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_READ_EXTERNAL_PERMISSION)
        {
            if(grantResults.length > 0)
            {
                int grantResult = grantResults[0];
                if(grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    // If user grant the permission then open browser let user select audio file.
                    //selectAudioFile();
                    myListAudio =  loadAudio();
                }else
                {
                    Toast.makeText(getApplicationContext(), "You denied read external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_exoplayer);

        if(myListAudio==null){
            Toast.makeText(this,"myListAudio == null",Toast.LENGTH_SHORT).show();
            myListAudio = new ArrayList<>();
            int readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(this, requirePermission, REQUEST_CODE_READ_EXTERNAL_PERMISSION);
                //myListAudio = loadAudio();
            } else myListAudio = loadAudio();

            mService = MainActivity.mAudioPlayService;
            if(myListAudio!=null && myListAudio.size()>0)
                mService.playSongsOffline(myListAudio,0);
            else    Toast.makeText(this,"myListAudio: null",Toast.LENGTH_SHORT).show();

        } else  Toast.makeText(this,"myListAudio != null",Toast.LENGTH_SHORT).show();

    }

    private void initializePlayer() {
        if (player == null ) {
            player = MainActivity.mAudioPlayService.getPlayer();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT >= 24) {
            initializePlayer();
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        if ((Util.SDK_INT < 24 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (player != null) {
//            playWhenReady = player.getPlayWhenReady();
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();

//            player.removeListener(playbackStateListener);

//            player.release();
            player = null;
        }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_online_exo_1:
                            selectedFragment = new playlistfrag();
                            break;
                        case R.id.nav_online_exo_2:
                            selectedFragment = new playingfrag();
                            break;
                        case R.id.nav_online_exo_3:
                            selectedFragment = new lyricfrag();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.containerfrag_online_exo,
                            selectedFragment).commit();

                    return true;
                }
            };


}
