package com.example.music;

import java.util.ArrayList;

public class OPComment {

    private     String At_time ; //the time posts this comment
    private   String Rating; //rating of this comment


    private   String idscm ; //id song comment
    private   String idsubscm ; //id sub song comment
    private   String iduser ; //iduser
    private   String idsong ; //id song

    private  String user; // username
    private String content; //noi dung
    private String avatar; //image avatar user
    private ArrayList<OPComment> subcomments;

    public OPComment(String user, String content, String avatar) {
        this.user = user;
        this.content = content;
        this.avatar = avatar;
        this.subcomments = new ArrayList<OPComment>();
    }
    public OPComment(String user, String content, String avatar, ArrayList<OPComment> subcomments) {
        this.user = user;
        this.content = content;
        this.avatar = avatar;
        this.subcomments = subcomments;
    }

    public OPComment(String at_time, String rating, String idscm, String idsubscm, String iduser, String idsong, String user, String content, String avatar) {
        At_time = at_time;
        Rating = rating;
        this.idscm = idscm;
        this.idsubscm = idsubscm;
        this.iduser = iduser;
        this.idsong = idsong;
        this.user = user;
        this.content = content;
        this.avatar = avatar;
        this.subcomments = new ArrayList<OPComment>();
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<OPComment> getSubcomments() {
        return subcomments;
    }

    public void setSubcomments(ArrayList<OPComment> subcomments) {
        this.subcomments = subcomments;
    }

    public void addAllSubcomments(ArrayList<OPComment> subcomments) {
        this.subcomments.addAll(subcomments);
    }

    public String getAt_time() {
        return At_time;
    }

    public void setAt_time(String at_time) {
        At_time = at_time;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getIdscm() {
        return idscm;
    }

    public void setIdscm(String idscm) {
        this.idscm = idscm;
    }

    public String getIdsubscm() {
        return idsubscm;
    }

    public void setIdsubscm(String idsubscm) {
        this.idsubscm = idsubscm;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getIdsong() {
        return idsong;
    }

    public void setIdsong(String idsong) {
        this.idsong = idsong;
    }
}
