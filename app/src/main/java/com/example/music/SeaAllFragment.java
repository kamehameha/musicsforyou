package com.example.music;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.music.model.AudioPlayService;
import com.example.music.model.Playlist;
import com.example.music.model.PostMan;
import com.example.music.model.Song;
import com.example.music.ui.MyPlayListViewAdapter;
import com.example.music.ui.MyPlayListViewAdapterSearch;
import com.example.music.ui.MySongListViewAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SeaAllFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SeaAllFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeaAllFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;

    public static SeaAllFragment instance ;

    public SeaAllFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SeaAllFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SeaAllFragment newInstance(String param1, String param2) {
        SeaAllFragment fragment = new SeaAllFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        instance = fragment;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }
    AudioPlayService mService;

    ArrayList<Song> listSong;

    MyListView listviewSong;

    MySongListViewAdapter songAdapter;
    ArrayList<Playlist> listPlaylist;

    MyListView listviewPlaylist ;

    MyPlayListViewAdapterSearch playlistAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sea_all, container, false);

         listSong = new ArrayList<>();




        listviewSong = view.findViewById(R.id.lv_seaallfrag_songs);

        songAdapter = new MySongListViewAdapter(getContext(), R.layout.row_mylistview_2, listSong);

        listviewSong.setAdapter(songAdapter);



          listPlaylist = new ArrayList<>();


          listviewPlaylist = view.findViewById(R.id.lv_seaallfrag_playlists);

          playlistAdapter = new MyPlayListViewAdapterSearch(getContext(), R.layout.row_mylistview_2, listPlaylist);

        listviewPlaylist.setAdapter(playlistAdapter);


        mService = MainActivity.mAudioPlayService;

        listviewSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                ArrayList<Song> playsong = new ArrayList<>();
                playsong.add(listSong.get(position));
                mService.playSongsOnline(playsong,0);

            }
        });

        listviewPlaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                ArrayList<Song> playlist = new ArrayList<>();
                String idpl = listPlaylist.get(position).getIdpl();

                PostMan.getSongsOfPlaylist(idpl, playlist, null, true, 0);

                //get song of playlist



            }
        });


        return view;
    }

    public ArrayList<Song> getListSong() {
        return listSong;
    }



    public void renewListSong(ArrayList<Song> listSong) {

        this.listSong.clear();
        this.listSong.addAll(listSong);

    }

    public MyListView getListviewSong() {
        return listviewSong;
    }



    public MySongListViewAdapter getSongAdapter() {
        return songAdapter;
    }


    public ArrayList<Playlist> getListPlaylist() {
        return listPlaylist;
    }

    public void renewListPlaylist(ArrayList<Playlist> listPlaylist) {

        this.listPlaylist.clear();
        this.listPlaylist.addAll(listPlaylist);
    }

    public MyListView getListviewPlaylist() {
        return listviewPlaylist;
    }

    public MyPlayListViewAdapterSearch getPlaylistAdapterSearch() {
        return playlistAdapter;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String str) {
        if (mListener != null) {
            mListener.onSearchAllFragmentInteraction(str);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSearchAllFragmentInteraction(String str);
    }
}
