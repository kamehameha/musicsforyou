package com.example.music;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.Playlist;
import com.example.music.model.Song;
import com.example.music.offline.MyUtil;
import com.example.music.offline.OfflineFrag;
import com.example.music.offline.OfflineFragTabAlbum;
import com.example.music.offline.OfflineFragTabPlaylist;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity
        implements SeaAllFragment.OnFragmentInteractionListener , SeaLocalFragment.OnFragmentInteractionListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SeaLocalFragment seaLocalFragment;
    SeaAllFragment seaAllFragment;
    ViewPagerAdapter adapter;

    //EditText edt_searchactiv_value;
    ImageButton sendsearch;

    SearchView searchView;

    ArrayList<Song> listSong;
    ArrayList<Playlist> listPlaylist;
    ArrayList<Song> listAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

//         sendsearch = findViewById(R.id.img_searchactiv_send);
//
//        sendsearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //if(tabLayout.getSelectedTabPosition()==0)
//                    //implementSearchAll();
//               // else
////                String searchText = searchView.getQuery().toString();
//                String searchText = searchView.getQuery().toString();
//                searchView.clearFocus();
//
//                implementSearchAll(searchText);
//                implementSearchLocal(searchText);
//
//            }
//        });



        //edt_searchactiv_value  = findViewById(R.id.edt_searchactiv_value);


         searchView = findViewById(R.id.searchview_searchactiv_value);
         searchView.requestFocus();
         searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
             @Override
             public boolean onQueryTextSubmit(String query) {

                 implementSearchAll(query);
                 implementSearchLocal(query);
                 searchView.clearFocus();
                 return false;
             }

             @Override
             public boolean onQueryTextChange(String newText) {
                 return false;
             }
         });

        listSong = new ArrayList<>();

        listAlbum= new ArrayList<>();

        listPlaylist = new ArrayList<>();


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);
    }

    private void implementSearchLocal( String value){
        if (value.compareTo("") == 0
        || value.compareTo(" ") == 0 )
            return;



        listSong.clear();
        ArrayList<Song> rootListSong  = OfflineFragTabPlaylist.mListSong;

        if(rootListSong==null || rootListSong.isEmpty()){
            Toast.makeText(this,"Offline: chưa có bài hát nào!",Toast.LENGTH_SHORT).show();
            return;
        }
        //Toast.makeText(this,String.valueOf(rootListSong.size()),Toast.LENGTH_SHORT).show();

        String textSearch = value;
        textSearch = MyUtil.removeAccent(textSearch);

        String songName;
        for(Song song  :  rootListSong){

            songName = song.getSongname().toLowerCase();
            songName = MyUtil.removeAccent(songName);

            if(songName.contains(textSearch))
                listSong.add(song);

        }
        if(listSong.isEmpty())
            Toast.makeText(this,"Offline: Không tìm thấy bài hát!",Toast.LENGTH_SHORT).show();

        //---------------------------------------------//
        //listAlbum.clear();

        listAlbum=getListAlbumByName(value);

        //goi frag hien thi
//                                songAdapter.notifyDataSetChanged();
        if (SeaLocalFragment.instance != null){
            //Toast.makeText(this,"Offline search instance !=null",Toast.LENGTH_SHORT).show();
            SeaLocalFragment.instance.renewListSong(listSong);
            SeaLocalFragment.instance.getSongAdapter().notifyDataSetChanged();

            SeaLocalFragment.instance.renewListAlbum(listAlbum);
            SeaLocalFragment.instance.getAlbumAdapter().notifyDataSetChanged();
            //SeaLocalFragment.instance.renewListPlaylist(listPlaylist);
            //SeaLocalFragment.instance.getPlaylistAdapter().notifyDataSetChanged();

        }
        //else  Toast.makeText(this,"Offline search instance ==null",Toast.LENGTH_SHORT).show();
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

        //test player manager
//                                SystemMusic.addSongsToPlaylingList(listSong);
//                                SystemMusic.playSongsList(listSong, 0);


    }

    private void implementSearchAll(String value) {

        if (value.compareTo("") == 0
                || value.compareTo(" ") == 0 )
            return;

        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/searchAllSource")
                .addBodyParameter("value",value)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONObject jo = response.getJSONObject("response");

                                String idpl;
                                String idsong ;
                                String songname;
                                String composer;
                                String singer ;
                                String genre ;
                                String img ;
                                String linkmp3 ;
                                String rating_ ;
                                String created_at ;
                                String iduser ;
                                String lyric ;

                                Song song;

                                listSong.clear();

                                JSONArray jsong = jo.getJSONArray("songs");

                                for (int i=0; i < jsong.length(); i++) {

                                    idsong = jsong.getJSONObject(i).getString("idsong");
                                    songname = jsong.getJSONObject(i).getString("songname");
                                    composer = jsong.getJSONObject(i).getString("composer");
                                    singer = jsong.getJSONObject(i).getString("singer");
                                    genre = jsong.getJSONObject(i).getString("genre");
                                    img = jsong.getJSONObject(i).getString("img");
                                    linkmp3 = jsong.getJSONObject(i).getString("linkmp3");
                                    rating_ = jsong.getJSONObject(i).getString("rating_");
                                    created_at = jsong.getJSONObject(i).getString("created_at");
                                    iduser = jsong.getJSONObject(i).getString("iduser");
                                    lyric = jsong.getJSONObject(i).getString("lyric");


                                    song = new Song( idsong, songname,
                                            composer, singer, genre, img,linkmp3,
                                            rating_, created_at, iduser, lyric);

                                    listSong.add(song);

                                }

//                                String idpl;
                                String plname ;
//                                String img;
                                String type;
//                                String rating_ ;
                                String plstatus ;
                                String shared ;
//                                String iduser ;
//                                String created_at ;

                                Playlist pl;

                                listPlaylist.clear();

                                JSONArray jplaylist = jo.getJSONArray("playlists");

                                for (int i=0; i < jplaylist.length(); i++) {
                                    idpl = jplaylist.getJSONObject(i).getString("idpl");
                                    plname = jplaylist.getJSONObject(i).getString("plname");
                                    img = jplaylist.getJSONObject(i).getString("img");
                                    type = jplaylist.getJSONObject(i).getString("type");
                                    rating_ = jplaylist.getJSONObject(i).getString("rating_");
                                    plstatus = jplaylist.getJSONObject(i).getString("status");
                                    shared = jplaylist.getJSONObject(i).getString("shared");
                                    iduser = jplaylist.getJSONObject(i).getString("iduser");
                                    created_at = jplaylist.getJSONObject(i).getString("created_at");


                                    pl = new Playlist(idpl, plname, img,
                                            type, rating_, plstatus, shared,iduser,
                                            created_at);

                                    listPlaylist.add(pl);

                                }


                                //goi frag hien thi
//                                songAdapter.notifyDataSetChanged();
                                if (SeaAllFragment.instance != null){
                                    SeaAllFragment.instance.renewListSong(listSong);
                                    SeaAllFragment.instance.getSongAdapter().notifyDataSetChanged();

                                    SeaAllFragment.instance.renewListPlaylist(listPlaylist);
                                    SeaAllFragment.instance.getPlaylistAdapterSearch().notifyDataSetChanged();

                                }
//                                Toast.makeText(OnlinePlayerActivity.this, " " + x + ", " + comContent, Toast.LENGTH_LONG).show();

                                //test player manager
//                                SystemMusic.addSongsToPlaylingList(listSong);
//                                SystemMusic.playSongsList(listSong, 0);
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Online: Không tìm thấy kết quả nào" , Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private ArrayList<Song> getListAlbumByName(String albumName){
        ArrayList<Song> resultList = new ArrayList<>();
        albumName = MyUtil.removeAccent(albumName.toLowerCase());
        String album;

        for(Song song: OfflineFragTabAlbum.listAlbumDistinctByName){
            album = MyUtil.removeAccent(song.getAlbum().toLowerCase());
            if(album.contains(albumName))
                resultList.add(song);
        }

        return resultList;
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        seaLocalFragment =  SeaLocalFragment.newInstance("no","no");
        seaAllFragment =  SeaAllFragment.newInstance("yes","yes");

        adapter.addFragment(seaAllFragment, "World");
        adapter.addFragment(seaLocalFragment, "Local");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSearchAllFragmentInteraction(String str) {

    }

    @Override
    public void onSearchLocalFragmentInteraction(String str) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
