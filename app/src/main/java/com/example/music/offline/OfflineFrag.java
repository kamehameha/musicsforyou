package com.example.music.offline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.example.music.MainActivity;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OfflineFrag extends Fragment {
    ViewPagerAdapter adapter;
    //private SeaLocalFragment seaLocalFragment;
    //private SeaAllFragment seaAllFragment;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    //private OfflineFragTabPlaylist offlineFragTab;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_offline_layout, container, false);

        tabLayout =  view.findViewById(R.id.offline_tabs);

         viewPager = (ViewPager) view.findViewById(R.id.offline_viewpager);

        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);



        return view;
    }

    private void setupViewPager() {
        adapter = new OfflineFrag.ViewPagerAdapter(getChildFragmentManager());
        //seaLocalFragment =  SeaLocalFragment.newInstance("no","no");
        //seaAllFragment =  SeaAllFragment.newInstance("yes","yes");
        //adapter.addFragment(seaAllFragment, "World");
        //adapter.addFragment(seaLocalFragment, "Local");
        //offlineFragTab = new OfflineFragTabPlaylist();
        adapter.addFragment(OfflineFragTabPlaylist.newInstance("yes","yes"),"Tất cả bài hát");
        adapter.addFragment(OfflineFragTabAlbum.newInstance("no","no"),"Albums");
        //adapter.addFragment(OfflineFragTabPlaylist.newInstance("ok","ok"),"lyric");
        viewPager.setAdapter(adapter);
    }



    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }


        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new OfflineFragTabPlaylist();
                case 1:
                    return new OfflineFragTabAlbum();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }




    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;

        //releasePlayer();
        //playerControlView.setPlayer(null);
        //LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
    }






    @Override
    public void onStart() {
        super.onStart();

        //Toast.makeText(getContext(),"onStart",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();


        //Toast.makeText(getContext(),"onResume",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }



}
