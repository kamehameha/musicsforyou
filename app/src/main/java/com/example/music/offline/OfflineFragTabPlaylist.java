package com.example.music.offline;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.util.Util;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class OfflineFragTabPlaylist extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OfflineFragTabPlaylist(){
        //
    }

    public static OfflineFragTabPlaylist instance ;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SeaLocalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OfflineFragTabPlaylist newInstance(String param1, String param2) {
        OfflineFragTabPlaylist fragment = new OfflineFragTabPlaylist();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        instance=fragment;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private static final int REQUEST_CODE_READ_EXTERNAL_PERMISSION = 2;

    private static SimpleExoPlayer player;
    private AudioPlayService mService;
    public static ArrayList<Song> mListSong;

    private ListView mListView;
    private ImageView mImageThumbAlbumArt;
    //private PlayerControlView playerControlView;

    private TextView tvSongNameOnPanel;
    private TextView tvSongArtistOnPanel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_offline_playlist, container, false);
        //Toast.makeText(getContext(),"on create",Toast.LENGTH_SHORT).show();


//        if(mListSong==null) {
//            //Toast.makeText(getContext(),"mListSong == null",Toast.LENGTH_SHORT).show();
//            mListSong = new ArrayList<>();
//            int readExternalStoragePermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
//            if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
//                String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
//                ActivityCompat.requestPermissions(getActivity(), requirePermission, REQUEST_CODE_READ_EXTERNAL_PERMISSION);
//                //mListSong = loadAudio();
//            } else {
//                //Toast.makeText(getContext(),"mListSong != null",Toast.LENGTH_SHORT).show();
//                mListSong = loadAudio();
//            }
//        }



        mService = MainActivity.mAudioPlayService;
//
        mListView = view.findViewById(R.id.offline_lvSong);
        mListView.setAdapter(new CustomListSongAdapter(getContext(),mListSong));
//
//        tvSongNameOnPanel = view.findViewById(R.id.songTitle_panel_control_view);
//        tvSongArtistOnPanel = view.findViewById(R.id.songArtist_panel_control_view);
//        tvSongNameOnPanel.setSelected(true);
//        tvSongArtistOnPanel.setSelected(true);
//
//        mImageThumbAlbumArt= view.findViewById(R.id.offline_thumb_AlbumArt);
//        mImageThumbAlbumArt.setFocusedByDefault(false);
//        mImageThumbAlbumArt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                turnOnMainPlaying();
//            }
//        });
//
//        final LinearLayout myLinear =  view.findViewById(R.id.offline_linear_panel_control_player_view);
//        myLinear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               turnOnMainPlaying();
//            }
//        });

        //playerControlView = view.findViewById(R.id.offline_fragtab_player_control_view);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mListSong!=null && mListSong.size()>0) {
                    //if (mService.isPlayingOnline()) {
                        mService.playSongsOffline(mListSong, i);

                      //Toast.makeText(getContext(),  mService.getPlayingSong().getAlbum(),Toast.LENGTH_SHORT).show();
                        //mService.getPlayer().setPlayWhenReady(false);
                    //} else
                    //    mService.playSongWithIndex(i);

                }

            }
        });
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playingfrag_playnewsong"));
        loadNewSongInfomation();
        return view;
    }

    private void turnOnMainPlaying(){
         if(mService.getPlayingList()==null)
             return;
         Intent intent;
         if(mService.isPlayingOnline())
             intent = new Intent(getActivity(), OnlineExoPlayerActivity.class);
         else
             intent = new Intent(getActivity(), OfflinePlayingActivity.class);
         //getActivity().finish();
         startActivity(intent);
     }
    public ArrayList<Song> getmListSong(){return mListSong;}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof SeaLocalFragment.OnFragmentInteractionListener) {
//            mListener = (SeaLocalFragment.OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
        //Toast.makeText(getContext(),"onAttach",Toast.LENGTH_SHORT).show();
    }

    private ArrayList<Song> loadAudio() {
        ContentResolver contentResolver = getContext().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);
        //audioList = new ArrayList<>();
        MyUtil myUtil = new MyUtil(getContext());
        boolean isFirstSaveAlbumImages = myUtil.loadStateSaveAlbumImages();
        ArrayList<Song> audioList = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            //MyUtil myutil = new MyUtil(getApplicationContext());

            int index = 0;
            Bitmap bm;
            while (cursor.moveToNext()) {

                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String composer = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.COMPOSER));
                String albumId = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                int duration = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                // Save to audioList
                if(!data.endsWith("flac") && !data.endsWith("FLAC")){
                    audioList.add(new Song(data, title, album, artist,duration,albumId));
                }

                if(isFirstSaveAlbumImages){
                    bm = myUtil.getAlbumArt(data);
                    if(bm!=null)
                        bm =Bitmap.createScaledBitmap(bm, 200, 200, false);
                    myUtil.saveImage(bm,title+artist+album);
                }
            }
            myUtil.storeStateSaveAlbumImages(false);
        }
        cursor.close();
        return audioList;
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_READ_EXTERNAL_PERMISSION)
        {
            if(grantResults.length > 0)
            {
                int grantResult = grantResults[0];
                if(grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    // If user grant the permission then open browser let user select audio file.
                    //selectAudioFile();
                    Toast.makeText(this.getContext(), "Start loading audio, Please wait along time!", Toast.LENGTH_LONG).show();
                    mListSong =  loadAudio();
                }else
                {
                    Toast.makeText(this.getContext(), "You denied read external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;

        releasePlayer();
        //playerControlView.setPlayer(null);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
    }




    private void initializePlayer() {
        if (player == null ) {
            player = MainActivity.mAudioPlayService.getPlayer();
            //player = mService.getPlayer();

            //playerControlView.setPlayer(player);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT >= 24) {
            initializePlayer();
        }
        //Toast.makeText(getContext(),"onStart",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        if ((Util.SDK_INT < 24 || player == null)) {
            initializePlayer();
        }
        //Toast.makeText(getContext(),"onResume",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (player != null) {
//            playWhenReady = player.getPlayWhenReady();
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();

//            player.removeListener(playbackStateListener);

//            player.release();
            player = null;
            //playerControlView.setPlayer(null);
        }
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            //Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            loadNewSongInfomation();
        }
    };
    private void loadNewSongInfomation(){

        if( mService.getPlayingList()==null )
            return;


//        if(!mService.isPlayingOnline())
//
//        Song song = mService.getPlayingSong();
//
//        Mp3File mp3file = null;
//        try {
//            mp3file = new Mp3File(song.getLinkmp3());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (UnsupportedTagException e) {
//            e.printStackTrace();
//        } catch (InvalidDataException e) {
//            e.printStackTrace();
//        }
//        if ( mp3file!=null && mp3file.hasId3v2Tag()){
//            ID3v2 id3v2Tag = mp3file.getId3v2Tag();
//            String lyric = id3v2Tag.getLyrics();
//
//            Toast.makeText(getContext(),lyric,Toast.LENGTH_SHORT).show();



//            System.out.println("Track: " + id3v2Tag.getTrack());
//            System.out.println("Artist: " + id3v2Tag.getArtist());
//            System.out.println("Title: " + id3v2Tag.getTitle());
//            System.out.println("Album: " + id3v2Tag.getAlbum());
//            System.out.println("Year: " + id3v2Tag.getYear());
//            System.out.println("Genre: " + id3v2Tag.getGenre() + " (" + id3v2Tag.getGenreDescription() + ")");
//            System.out.println("Comment: " + id3v2Tag.getComment());
//            System.out.println("Lyrics: " + id3v2Tag.getLyrics());
//            System.out.println("Composer: " + id3v2Tag.getComposer());
//            System.out.println("Publisher: " + id3v2Tag.getPublisher());
//            System.out.println("Original artist: " + id3v2Tag.getOriginalArtist());
//            System.out.println("Album artist: " + id3v2Tag.getAlbumArtist());
//            System.out.println("Copyright: " + id3v2Tag.getCopyright());
//            System.out.println("URL: " + id3v2Tag.getUrl());
//            System.out.println("Encoder: " + id3v2Tag.getEncoder());
//            byte[] albumImageData = id3v2Tag.getAlbumImage();
//            if (albumImageData != null) {
//                System.out.println("Have album image data, length: " + albumImageData.length + " bytes");
//                System.out.println("Album image mime type: " + id3v2Tag.getAlbumImageMimeType());
//            }
//        }


    }
}
