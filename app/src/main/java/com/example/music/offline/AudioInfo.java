package com.example.music.offline;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;

public class AudioInfo extends MediaMetadataRetriever {
    MediaMetadataRetriever metaRetriever;
    public AudioInfo(Context ctx, Uri yourURi){
        metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(ctx,yourURi);
    }
    public String getTitile(){
        return metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    }
    public String getArtist(){
        return metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
    }
    public String getAlbum(){
        return metaRetriever.extractMetadata((MediaMetadataRetriever.METADATA_KEY_ALBUM));
    }

}
