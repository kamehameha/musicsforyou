package com.example.music.offline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import java.io.IOException;

public class OfflinePlayingActivity extends AppCompatActivity {

    private PlayerControlView playerControlView;
    private AudioPlayService mService;
    private SimpleExoPlayer player;

    private TextView tvSongName;
    private TextView tvArtist;
    private TextView tvAlbum;
    private TextView tvShowOnTouch;
    private ImageButton btnShowPlayingList;
    private ImageButton btnShowLyrics;

    private CircularImageView imgAlbumArt;

    private TextView tvLyric;

    RelativeLayout relativeLayoutScrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_playing_activity);

        mService=MainActivity.mAudioPlayService;
        //player = mService.getPlayer();

        relativeLayoutScrollView = findViewById(R.id.relative_scroll_view_lyric);


        tvLyric = findViewById(R.id.tv_lyric);

        tvSongName = findViewById(R.id.offline_songName);
        tvArtist = findViewById(R.id.offline_songArtist);
        tvAlbum = findViewById(R.id.offline_songAlbum);
        tvShowOnTouch=findViewById(R.id.offline_tvShowOnTouch);


        imgAlbumArt = findViewById(R.id.offline_albumArt);
        btnShowPlayingList = findViewById(R.id.offline_btn_show_playing_list);
        btnShowPlayingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),OfflinePlayingListSongActivity.class);
                startActivity(intent);
            }
        });
        btnShowLyrics=findViewById(R.id.offline_btn_show_lyrics);
        btnShowLyrics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(relativeLayoutScrollView.getVisibility()==View.INVISIBLE){
                    relativeLayoutScrollView.setVisibility(View.VISIBLE);
                    imgAlbumArt.setVisibility(View.INVISIBLE);
                }
                else{
                    relativeLayoutScrollView.setVisibility(View.INVISIBLE);
                    //relativeLayoutScrollView.setVisibility(View.INVISIBLE);
                    imgAlbumArt.setVisibility(View.VISIBLE);
                }

            }
        });

        playerControlView = findViewById(R.id.offline_player_control_view);


        loadNewSongInfomation();

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playingfrag_playnewsong"));


        displayMetrics = new DisplayMetrics();
        this.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        screenHeight=displayMetrics.heightPixels;
        screenWidth=displayMetrics.widthPixels;


        final RelativeLayout rootLayout = findViewById(R.id.offline_playing_root_layout);
        rootLayout.setOnTouchListener(onTouchListener);
    }


    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            int X = (int) motionEvent.getX();
            int Y = (int) motionEvent.getY();
            int eventaction = motionEvent.getAction();
            switch (eventaction) {
                case MotionEvent.ACTION_DOWN:
                    oldY=Y;
                    oldX = X;
                    isTouch = true;

                    if(X<screenWidth/2) isSlingOnLeftScreen=true;
                    else isSlingOnLeftScreen=false;

                    if(player.isPlaying())
                        isPlaying=true;
                    currentPos = player.getCurrentPosition();

                    break;

                case MotionEvent.ACTION_MOVE:
                    //Toast.makeText(this, "Finger move coordinate " + "X: " + X + " Y: " + Y, Toast.LENGTH_SHORT).show();
                    doOnTouchMove(X,Y);
                    break;

                case MotionEvent.ACTION_UP:
                    isTouch=false;

                    moveCount=0;
                    //tvVolume.setVisibility(View.INVISIBLE);
                    tvShowOnTouch.setVisibility(View.INVISIBLE);

                    if(isMediaTimeChanging){
                        player.seekTo(currentPos);
                        isMediaTimeChanging=false;
                    }


                    if(isPlaying) {
                        player.setPlayWhenReady(true);
                        isPlaying=false;
                    }
                    break;
            }
            return true;

        }
    };

    // Các thuộc tính dưới đây để thao tác tua nhạc và âm lượng trong sự kiện OnTouchMove
    private int oldX = 0;  //
    private int oldY = 0;  //
    private boolean isTouch = false;
    private int screenHeight;
    private int screenWidth;
    private boolean isSlingOnLeftScreen = false;
    private boolean isPlaying = false; // flag đánh dấu thao tác trong timebar và kéo trên màn hình
    private boolean isMediaTimeChanging = false;
    private int moveCount = 0;
    private int totalTimeSpan = 0;
    private int totalVolumeSpan = 0;
    private int countToSetVolume=0;
    private static int currVolume = 20;
    private final static int maxVolume = 20;
    private DisplayMetrics displayMetrics;
    private long currentPos;
    //

    @Override
    public void onStart() {
        super.onStart();

        //player = MainActivity.mAudioPlayService.getPlayer();
        player = mService.getPlayer();
        //playerView.setPlayer(player);
        playerControlView.setPlayer(player);


    }

    @Override
    public void onResume() {
        super.onResume();
        //hideSystemUi();
    }


    @Override
    public void onStop() {

        if(player!=null)
            MyUtil.storePosition((int)player.getCurrentPosition());
        super.onStop();
    }


    @Override
    public void onDestroy() {


        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        //subtitles = null;

        //playerView = null;

        playerControlView.setPlayer(null);

        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.i("sendMessageToPlayingFrag_receiver", "Đã nhận");
            loadNewSongInfomation();
        }
    };

    private void loadNewSongInfomation(){
        Song song  = mService.getPlayingSong();

        tvSongName.setText(song.getSongname());
        tvArtist.setText(song.getSinger());
        tvAlbum.setText(song.getAlbum());

        tvSongName.setSelected(true);
        tvArtist.setSelected(true);
        tvAlbum.setSelected(true);

        MyUtil myutil = new MyUtil(getApplicationContext());
        Bitmap bm = myutil.getAlbumArt(song.getLinkmp3());
        if(bm==null)
          bm =  BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
        imgAlbumArt.setImageBitmap(bm);
        //else imgAlbumArt.setImageResource(R.drawable.defaultimage);

        RelativeLayout relative = (RelativeLayout) findViewById(R.id.offline_playing_root_layout);
        bm =Bitmap.createScaledBitmap(bm, 100, 100, false);
        Drawable draw = new BitmapDrawable(bm);
        (relative).setBackgroundDrawable(draw);

        relativeLayoutScrollView.setVisibility(View.INVISIBLE);
        imgAlbumArt.setVisibility(View.VISIBLE);
        loadLyrics(mService.getPlayingSong());

        MyUtil.rotateImage(imgAlbumArt,5000);



    }
    private String lyric="";
    private void loadLyrics(Song song){

        long duration  = song.getDuration();
        if(duration>10*60*1000)
            return;
        Mp3File mp3File = null;
        try {
            mp3File = new Mp3File(song.getLinkmp3());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedTagException e) {
            e.printStackTrace();
        } catch (InvalidDataException e) {
            e.printStackTrace();
        }
        if(mp3File==null || mp3File.getId3v2Tag() == null ){
            btnShowLyrics.setVisibility(View.INVISIBLE);
            return;
        }
        lyric = mp3File.getId3v2Tag().getLyrics();
        if(lyric==null ||  lyric.compareTo("")==0)
            btnShowLyrics.setVisibility(View.INVISIBLE);
        else{
            btnShowLyrics.setVisibility(View.VISIBLE);
        }
        tvLyric.setText(lyric);
        //tvLyric.setVisibility(View.INVISIBLE);
    }

    private void doOnTouchMove(int X, int Y){ // con me no
        moveCount++;
        int timeSpan = X-oldX;
        int volumeSpan = Y-oldY;
        if(moveCount<6) {
            totalTimeSpan += Math.abs(timeSpan);
            totalVolumeSpan += Math.abs(volumeSpan);
            if(moveCount==5) {
                if (totalTimeSpan * 3 > totalVolumeSpan) {
                    isMediaTimeChanging = true;
                    player.setPlayWhenReady(false);
                    totalTimeSpan = 0;
                    totalVolumeSpan = 0;
                }
            }
        }
        else{
            if (isMediaTimeChanging) {
                //currentPos = player.getCurrentPosition();
                long duration = player.getDuration();
                timeSpan *= 50;
                currentPos += timeSpan;

                if (currentPos > duration)
                    currentPos = duration;

                else if (currentPos < 0)
                    currentPos = 0;

//                player.seekTo(currentPos);
                tvShowOnTouch.setVisibility(View.VISIBLE);
                UpdatePositionWhilePlaying(currentPos);
            }
            //
            else {
                if (volumeSpan < 0) {
//                    if(!isSlingOnLeftScreen) {}//countToShowListView++;
//                    else {
                        countToSetVolume++;
                        if(countToSetVolume==4){
                            currVolume +=1;
                            if (currVolume >= maxVolume)
                                currVolume = maxVolume;
                            countToSetVolume=0;
                        }
                    //}


                } else if (volumeSpan > 0) {
//                    if(!isSlingOnLeftScreen) {}//countToShowListView--;
//                    else{
                        countToSetVolume++;
                        if(countToSetVolume==4){
                            currVolume -=1;
                            if (currVolume <= 0)
                                currVolume = 0;
                            countToSetVolume=0;
                        }
                    //}

                }

                final float volume = (float) (1 - (Math.log(maxVolume - currVolume) / Math.log(maxVolume)));
                player.setVolume(volume);
                //
                //if(isSlingOnLeftScreen) {
                    tvShowOnTouch.setText(currVolume + "|" + maxVolume);
                    tvShowOnTouch.setVisibility(View.VISIBLE);
                //}
            }
        }
        oldX = X;
        oldY = Y;
    }

    private void UpdatePositionWhilePlaying(long newProgress){
        tvShowOnTouch.setText(ConvertTime(newProgress));
        //tvCurrentDuration.setText(ConvertTime(newProgress));
        //timeBar.setProgress(newProgress);
    }
    String ConvertTime(long time){
        String result;
        time/=1000;
        if (time < 0)
            return "00:00";
        if (time / 3600 > 0)
            //result = String.Format("{0:00}:{1:00}:{2:00}", time / 3600, (time / 60) % 60, time % 60);
            result = String.format("%02d:%02d:%02d", time / 3600, (time / 60) % 60, time % 60);
        else
            //result = String.Format("{0:00}:{1:00}", (time / 60) % 60, time % 60);
            result = String.format("%02d:%02d",(time / 60) % 60, time % 60);

        return result;
    }
}
