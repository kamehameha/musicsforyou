package com.example.music.offline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;

import java.util.ArrayList;

public class OfflinePlayingListSongActivity extends AppCompatActivity {

    private SimpleExoPlayer player;
    private AudioPlayService mService;
    private ArrayList<Song> listPlayingSong;


    private PlayerControlView playerControlView;
    private ImageView mImageThumbAlbumArt;
    private TextView tvSongNameOnPanel;
    private TextView tvSongArtistOnPanel;
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_playing_listsong_layout);


        mService = MainActivity.mAudioPlayService;

        listView=findViewById(R.id.offline_lv_playing_list_song);

        tvSongNameOnPanel = findViewById(R.id.offline_playing_listsong_layout_title);
        tvSongArtistOnPanel = findViewById(R.id.offline_playing_listsong_layout_artist);
        tvSongNameOnPanel.setSelected(true);
        tvSongArtistOnPanel.setSelected(true);

        mImageThumbAlbumArt= findViewById(R.id.offline_playing_listsong_layout_imgAlbum);

        mImageThumbAlbumArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                turnOnMainPlaying();

            }
        });

        final LinearLayout controlView = findViewById(R.id.offline_playing_listsong_layout_control_view_info);
        controlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                turnOnMainPlaying();

            }
        });

        playerControlView = findViewById(R.id.offline_playing_listsong_layout_player_control_view);



        listPlayingSong=mService.getPlayingList();

        CustomListSongAdapter customListSongAdapter = new CustomListSongAdapter(getApplicationContext(),listPlayingSong);
        listView.setAdapter(customListSongAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mService.playSongWithIndex(i);
            }
        });

        loadNewSongInfomation();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playingfrag_playnewsong"));

    }

    @Override
    protected void onStart() {
        super.onStart();
        player = mService.getPlayer();
        playerControlView.setPlayer(player);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        //subtitles = null;

        //playerView = null;

        playerControlView.setPlayer(null);
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            loadNewSongInfomation();
        }
    };

    private  void loadNewSongInfomation (){
        if( mService.getPlayingList()==null )
            return;
        //Toast.makeText(this,"loadNewSongInfo",Toast.LENGTH_SHORT).show();

        Song song = mService.getPlayingSong();
        MyUtil myutil = new MyUtil(getApplicationContext());
        Bitmap bm = myutil.getAlbumArt(song.getLinkmp3());
        if (bm == null)
            bm = BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
        mImageThumbAlbumArt.setImageBitmap(bm);

        tvSongNameOnPanel.setText(song.getSongname());
        tvSongArtistOnPanel.setText(song.getSinger());

//
        RelativeLayout relative = (RelativeLayout) findViewById(R.id.offline_playing_list_song_layout_root);
        bm =Bitmap.createScaledBitmap(bm, 100, 100, false);
        Drawable draw = new BitmapDrawable(bm);
        (relative).setBackgroundDrawable(draw);

        MyUtil.rotateImage(mImageThumbAlbumArt,3000);

        if(!mService.isPlayingOnline())
            listView.setSelection(mService.getPlayingPositionIndex());

    }

    private void turnOnMainPlaying(){
        if(mService.getPlayingList()==null)
            return;
        Intent intent;
        if(mService.isPlayingOnline())
            intent = new Intent(this, OnlineExoPlayerActivity.class);
        else
            intent = new Intent(this, OfflinePlayingActivity.class);
        //getActivity().finish();
        startActivity(intent);
    }
}
