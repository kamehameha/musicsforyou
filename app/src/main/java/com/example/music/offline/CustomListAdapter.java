package com.example.music.offline;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.music.R;

import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter {
    private ArrayList<Audio> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    MyUtil myUtil;

    public CustomListAdapter(Context aContext,  ArrayList<Audio> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        myUtil = new MyUtil(context);
    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listview_item_layout, null);
            holder = new ViewHolder();
            holder.flagView = (ImageView) convertView.findViewById(R.id.imageView_Item);
            holder.tvTitle= (TextView) convertView.findViewById(R.id.textView_Title);
            holder.tvArtist = (TextView) convertView.findViewById(R.id.textView_Artist);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // bên dưới là set tên bài hát, tên nghệ sĩ, ảnh album cho mỗi item
        Audio audio = this.listData.get(position);
        holder.tvTitle.setText(audio.getTitle());
        holder.tvArtist.setText(audio.getArtist());
        //
        Bitmap bm;
        String name = audio.getTitle() + audio.getArtist() +audio.getAlbumID();//.substring(0, .lastIndexOf('.'))
        bm= myUtil.getAlbumArt(audio.getData());// myUtil.loadImageBitmap(name);
        if(bm!=null)  holder.flagView.setImageBitmap(bm);
        else {
            holder.flagView.setImageResource(R.drawable.defaultimage); // nếu bm == null thì set ảnh mặc định
        }
        return convertView;
    }
    static class ViewHolder {
        //các thuộc tính tương ứng với các view trong file listview_item_layout.xml
        ImageView flagView;
        TextView tvTitle;
        TextView tvArtist;
    }
}
