package com.example.music.offline;

import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.music.model.Song;
import com.example.music.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.security.PublicKey;
import java.util.ArrayList;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class MyUtil {
    private final String STORAGE = " com.valdioveliu.valdio.audioplayer.STORAGE";
    // dưới đây là tên các value setting để store
    private static final String audioStoreName = "recentAudio";
    private static final String positionStoreName = "recentPosition";
    private final String shuffleStoreName = "recentShuffle";
    private final String playbackStateStoreName = "recentPlayBackState";
    private final String volumeStoreName = "recentVolume";
    private final String stateSaveAlbumImages = "stateSaveAlbumImage";

    private static final String audioListStoreName = "recentAudioList";


    private final String userStoreName = "recentUser";

    private static SharedPreferences preferences;
    private Context context;


    public MyUtil(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
    }



    public static void storeAudio(Song audio){
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(audio);
        editor.putString(audioStoreName,json);
        editor.apply();
    }
    public static Song loadAudio(){
        Gson gson = new Gson();
        String json = preferences.getString(audioStoreName, null);
        Type type = new TypeToken<Song>(){}.getType();
        return gson.fromJson(json,type);
    }

    public  void storeListSong(ArrayList<Song> arrayList) {

        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        editor.putString(audioListStoreName, json);
        editor.apply();
    }

    public  ArrayList<Song> loadListSong() {
        Gson gson = new Gson();
        String json = preferences.getString(audioListStoreName, null);
        Type type = new TypeToken<ArrayList<Song>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void storeUser(User user){
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();

        if (user == null){

            String json = gson.toJson(new User("1", "Anonymous", "img/4.jpg", "normal", "2019-10-02 22:06:53"));

            editor.putString(userStoreName,json);
            editor.apply();
        } else {

            String json = gson.toJson(user);

            editor.putString(userStoreName,json);
            editor.apply();
        }


    }
    public User loadUser(){
        Gson gson = new Gson();
        String json = preferences.getString(userStoreName, null);
        Type type = new TypeToken<User>(){}.getType();
        return gson.fromJson(json,type);
    }

    public static void storePosition(int position){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(positionStoreName,position);
        editor.apply();
    }
    public static int loadPosition(){
        return preferences.getInt(positionStoreName,0);
    }
    public void storeShuffle(boolean shuffle){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(shuffleStoreName,shuffle);
        editor.apply();
    }
    public Boolean loadShuffle(){
        return preferences.getBoolean(shuffleStoreName,false);
    }
    public void storePlayBackState(int playback){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(playbackStateStoreName,playback);
        editor.apply();
    }
    public int loadPlayBackState(){
      return  preferences.getInt(playbackStateStoreName,0);
    }
    public void storeVolume(int value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(volumeStoreName,value);
        editor.apply();
    }

    public boolean loadStateSaveAlbumImages(){
        return preferences.getBoolean(stateSaveAlbumImages,true);
    }
    public void storeStateSaveAlbumImages(boolean isFirstSave){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(stateSaveAlbumImages,false);
        editor.apply();
    }

    public int loadVolume(){
        return  preferences.getInt(volumeStoreName,20);
    }
    //
    public int TimNhiPhan(ArrayList<Audio> list, Audio value, int l, int r)
    {
        String songName = value.getTitle();
        String songURL = value.getData();

        while (r >= l)
        {
            int mid = l + (r - l) / 2;

            if (list.get(mid).getTitle().equals(songName) ) // tên giống nhau
            {
                if (list.get(mid).getData().equals(songURL)) //url giống nhau
                    return mid;
                // nếu không thì tìm tuyến tính từ mid+1 đến r
                int _i;
                for (_i = mid + 1; _i <= r; _i++)
                {
                    if (!list.get(_i).getTitle().equals(songName))
                        break;
                    if (list.get(_i).getData().equals(songURL))
                        return _i;
                }
                // nếu không thì tìm tuyến tính từ mid-1 đến l
                for (_i = mid - 1; _i >= l; _i--)
                {
                    if (!list.get(_i).getTitle().equals(songName))
                        break;
                    if (list.get(_i).getData().equals(songURL))
                        return _i;
                }
                return -1;
            }
            if (list.get(mid).getTitle().compareTo( songName) >0)
                r = mid - 1;
            else if (list.get(mid).getTitle().compareTo( songName) <0)
                l = mid + 1;
        }
        //
        return -1;
    }
    public Bitmap getAlbumArt_2(int album_id){
        Bitmap bm = null;
        try
        {
            final Uri sArtworkUri = Uri
                    .parse("content://media/external/audio/albumart");

            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);

            ParcelFileDescriptor pfd = context.getContentResolver()
                    .openFileDescriptor(uri, "r");
            //

            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (Exception e) {
        }
        return bm;
    }
    public Bitmap getAlbumArt(String mediaPath){
        Bitmap bm = null;
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mediaPath);

        byte [] data = mmr.getEmbeddedPicture();
        //coverart is an Imageview object
        // convert the byte array to a bitmap
        if(data != null)
        {
            bm = BitmapFactory.decodeByteArray(data, 0, data.length);
        }
       return  bm;
    }
    public void saveImage( Bitmap bitmap, String name){
        name = name + ".jpg";
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90,fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap loadImageBitmap(String name){
        name = name + ".jpg";
        FileInputStream fileInputStream;
        Bitmap bitmap = null;
        try{
            fileInputStream = context.openFileInput(name);
            bitmap = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    public int getRawResIdByName(String resName, String folder)  {
        String pkgName = context.getPackageName();
        // Return 0 if not found.
        // Trả về 0 nếu không tìm thấy.
        int resID = context.getResources().getIdentifier(resName, folder, pkgName);
        return resID;
    }

    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static void rotateImage(final ImageView image, long duration){

        image.clearAnimation();
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(duration);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatCount(RotateAnimation.ABSOLUTE);
        image.startAnimation(rotate);

        Log.i("rotation","runing");
    }
}
