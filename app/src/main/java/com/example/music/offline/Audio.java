package com.example.music.offline;

import java.io.Serializable;

public class Audio implements Serializable {
    private String data; //URI of Audio
    private String title;
    private String album;
    private String artist;
    private int albumID;// Dùng để load image của album bài hát, trong phương thức getAlbumArt_2
                        // ở class MyUtil
    //private int indexSortedByTitle;
    public Audio(String data, String title, String album, String artist, int id) {
        this.data = data;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.albumID=id;
        //this.indexSortedByTitle=indexSorted;
    }
//    public int getIndexSortedByTitle(){
//        return this.indexSortedByTitle;
//    }

    public int getAlbumID(){
        return albumID;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

}
