package com.example.music.offline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.music.MainActivity;
import com.example.music.OnlineExoPlayerActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OfflineListSongAlbumActivity extends AppCompatActivity {

    private ImageView imgAlbum;
    private TextView tvAlbumName;
    private ListView listView;

    private SimpleExoPlayer player;

    private AudioPlayService mService;

    private PlayerControlView playerControlView;

    private ImageView mImageThumbAlbumArt;
    private TextView tvSongNameOnPanel;
    private TextView tvSongArtistOnPanel;

    private ArrayList<Song> listSongsOfAlbum;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.offline_listsong_album_layout);

        Song song = (Song)getIntent().getSerializableExtra("albumInfo");


        //Toast.makeText(this,"Album: "+ song.getAlbum(),Toast.LENGTH_SHORT).show();

        listSongsOfAlbum = getSongOfAlbum(song.getAlbum());



        mService = MainActivity.mAudioPlayService;

        imgAlbum = findViewById(R.id.offline_listsong_album_imgAlbum);
        tvAlbumName=findViewById(R.id.offline_listsong_album_tvAlbumName);
        listView=findViewById(R.id.offline_listsong_album_lvSongsOfAlbum);

        tvSongNameOnPanel = findViewById(R.id.offline_listsong_album__songTitle_panel_control_view);
        tvSongArtistOnPanel = findViewById(R.id.offline_listsong_album__songArtist_panel_control_view);
        tvSongNameOnPanel.setSelected(true);
        tvSongArtistOnPanel.setSelected(true);

        mImageThumbAlbumArt= findViewById(R.id.offline_listsong_album_thumb_AlbumArt_panel_control_view);

        mImageThumbAlbumArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                turnOnMainPlaying();
            }
        });

        final LinearLayout controlView = findViewById(R.id.offline_listsong_album_linear_panel_control_view);
        controlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                turnOnMainPlaying();
            }
        });

        playerControlView = findViewById(R.id.offline_listsong_album__player_control_view);


        Bitmap bm = new MyUtil(getApplicationContext()).getAlbumArt(song.getLinkmp3());
        if(bm==null)
            imgAlbum.setImageResource(R.drawable.defaultimage);
        else
            imgAlbum.setImageBitmap(bm);

        tvAlbumName.setText(song.getAlbum());

        CustomListSongAdapter customListSongAdapter = new CustomListSongAdapter(getApplicationContext(),listSongsOfAlbum);
        listView.setAdapter(customListSongAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mService.playSongsOffline(listSongsOfAlbum,i);
            }
        });

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("playingfrag_playnewsong"));
        loadNewSongInfomation();
    }

    private ArrayList<Song> getSongOfAlbum(String album){
        ArrayList<Song> songsOfAlbum = new ArrayList<>();
        ArrayList<Song> rootList = OfflineFragTabPlaylist.mListSong;

        for(Song song:rootList){
            if(song.getAlbum().equals(album))
                songsOfAlbum.add(song);
        }

        return songsOfAlbum;

    }

    @Override
    protected void onStart() {
        super.onStart();
        player = mService.getPlayer();
        playerControlView.setPlayer(player);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        //subtitles = null;

        //playerView = null;

        playerControlView.setPlayer(null);
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            loadNewSongInfomation();
        }
    };

    private  void loadNewSongInfomation (){
        if( mService.getPlayingList()==null )
            return;
        //Toast.makeText(this,"loadNewSongInfo",Toast.LENGTH_SHORT).show();
        Bitmap bm=null;
        Song song = mService.getPlayingSong();
        if(mService.isPlayingOnline()){
            Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + mService.getPlayingSong().getImg()).into(mImageThumbAlbumArt);
        }
        else {
            MyUtil myutil = new MyUtil(getApplicationContext());
            bm = myutil.getAlbumArt(song.getLinkmp3());
            if (bm == null)
                bm = BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
            mImageThumbAlbumArt.setImageBitmap(bm);

            RelativeLayout relative = (RelativeLayout) findViewById(R.id.offline_listsong_album_root_layout);
            bm =Bitmap.createScaledBitmap(bm, 100, 100, false);
            Drawable draw = new BitmapDrawable(bm);
            (relative).setBackgroundDrawable(draw);

            MyUtil.rotateImage(mImageThumbAlbumArt,3000);

        }
        tvSongNameOnPanel.setText(song.getSongname());
        tvSongArtistOnPanel.setText(song.getSinger());

    }

    private void turnOnMainPlaying(){
        if(mService.getPlayingList()==null)
            return;
        Intent intent;
        if(mService.isPlayingOnline())
            intent = new Intent(this, OnlineExoPlayerActivity.class);
        else
            intent = new Intent(this, OfflinePlayingActivity.class);
        //getActivity().finish();
        startActivity(intent);
    }
}

