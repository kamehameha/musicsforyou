package com.example.music.offline;

public class OfflineAlbum {

    private String albumName;
    private String linkmp3;

    public OfflineAlbum(String albumName, String linkmp3){
        this.albumName=albumName;
        this.linkmp3=linkmp3;
    }

    public OfflineAlbum(){}

    public  String getAlbumName() {
        return albumName;
    }

    public String getLinkmp3() {
        return linkmp3;
    }
}
