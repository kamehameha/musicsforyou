package com.example.music.offline;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.music.R;
import com.example.music.model.Song;

import java.util.ArrayList;

public class CustomListSongAdapter extends BaseAdapter {
    private ArrayList<Song> listData;
    private LayoutInflater layoutInflater;
    private Context context;
   // private int resource;
    MyUtil myUtil;

    public CustomListSongAdapter(Context aContext,  ArrayList<Song> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        //this.resource=resource;
        myUtil = new MyUtil(context);
    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomListSongAdapter.ViewHolder holder;
        if (convertView == null) {
            //convertView = layoutInflater.inflate(R.layout.listview_item_layout, parent, false);
           // convertView = LayoutInflater.from(parent.getContext()).inflate(resource, parent,false);
            convertView = layoutInflater.inflate(R.layout.listview_item_layout, null);
            holder = new CustomListSongAdapter.ViewHolder();
            holder.flagView = (ImageView) convertView.findViewById(R.id.imageView_Item);
            holder.tvTitle= (TextView) convertView.findViewById(R.id.textView_Title);
            holder.tvArtist = (TextView) convertView.findViewById(R.id.textView_Artist);
            holder.tvDuration=(TextView)convertView.findViewById(R.id.textView_duration);
            convertView.setTag(holder);
        } else {
            holder = (CustomListSongAdapter.ViewHolder) convertView.getTag();
        }

        // bên dưới là set tên bài hát, tên nghệ sĩ, ảnh album cho mỗi item
        Song audio = this.listData.get(position);
        holder.tvTitle.setText(audio.getSongname());
        holder.tvArtist.setText(audio.getSinger());
        holder.tvDuration.setText(ConvertTime(audio.getDuration()));
        //
        Bitmap bm;
        String name = audio.getSongname() + audio.getSinger() +audio.getAlbum();//.substring(0, .lastIndexOf('.'))
        bm= myUtil.loadImageBitmap(name); // myUtil.getAlbumArt(audio.getData());
        if(bm == null) {
            bm = myUtil.getAlbumArt(audio.getLinkmp3());
            if (bm == null)
                holder.flagView.setImageResource(R.drawable.defaultimage);
            else myUtil.saveImage(bm,name);
        }
        else holder.flagView.setImageBitmap(bm);


        return convertView;
    }
    static class ViewHolder {

        //các thuộc tính tương ứng với các view trong file listview_item_layout.xml
        TextView tvDuration;
        ImageView flagView;
        TextView tvTitle;
        TextView tvArtist;
    }
    private String ConvertTime(long time){
        String result;
        time/=1000;
        if (time < 0)
            return "00:00";
        if (time / 3600 > 0)
            //result = String.Format("{0:00}:{1:00}:{2:00}", time / 3600, (time / 60) % 60, time % 60);
            result = String.format("%02d:%02d:%02d", time / 3600, (time / 60) % 60, time % 60);
        else
            //result = String.Format("{0:00}:{1:00}", (time / 60) % 60, time % 60);
            result = String.format("%02d:%02d",(time / 60) % 60, time % 60);

        return result;
    }
}
