package com.example.music.offline;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.music.MainActivity;
import com.example.music.R;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.squareup.picasso.Picasso;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OfflineFragTabAlbum extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public OfflineFragTabAlbum(){
        //
    }

    public static OfflineFragTabAlbum instance ;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SeaLocalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OfflineFragTabAlbum newInstance(String param1, String param2) {
        OfflineFragTabAlbum fragment = new OfflineFragTabAlbum();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        instance=fragment;
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    public static ArrayList<Song> listAlbumDistinctByName;


    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public static ArrayList<Song> makeListAlbumDistinctByName(){

        ArrayList<Song> rootOfflineListSong = OfflineFragTabPlaylist.mListSong;

        //ArrayList<Song> arrayListAlbum = new ArrayList<>();

        ArrayList<Song> offlineAlbums = new ArrayList<>(
                rootOfflineListSong.stream().filter(distinctByKey(Song::getAlbum)).collect(Collectors.toList()));

        return offlineAlbums;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_offline_album, container, false);



//        Set<String> set = new HashSet<>(arrayListAlbum.size());
//         listAlbumDistinctByName = new ArrayList<>(
//                arrayListAlbum.stream().filter(p -> set.add(p.getAlbumName())).collect(Collectors.toList()));
//

        if(listAlbumDistinctByName==null)
            listAlbumDistinctByName = makeListAlbumDistinctByName();

//        int i = 0;
//        for(Song song : listAlbumDistinctByName){
//            Log.i("nhc",i++ + "  "+song.getAlbum() + " | " +song.getLinkmp3());
//        }

        final GridView gridViewAlbum = (GridView)view.findViewById(R.id.offline_gridview_album);
        gridViewAlbum.setAdapter(new CustomGridAlbumAdapter(getContext(),listAlbumDistinctByName));

        gridViewAlbum.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Song song = (Song)gridViewAlbum.getItemAtPosition(i);
                ArrayList<Song> songsOfAlbum = getSongOfAlbum(song.getAlbum());

//                int index = 0;
//                for(Song s: songsOfAlbum){
//                    Log.i("Song",index++ + ": " +song.getAlbum() +" | " +s.getSongname());
//                }

                Intent intent = new Intent(getActivity(),OfflineListSongAlbumActivity.class);
                intent.putExtra("albumInfo",song);
                startActivity(intent);

            }
        });


        return view;
    }

    private ArrayList<Song> getSongOfAlbum(String album){
        ArrayList<Song> songsOfAlbum = new ArrayList<>();
        ArrayList<Song> rootList = OfflineFragTabPlaylist.mListSong;

        for(Song song:rootList){
            if(song.getAlbum().equals(album))
                songsOfAlbum.add(song);
        }

        return songsOfAlbum;

    }


}
