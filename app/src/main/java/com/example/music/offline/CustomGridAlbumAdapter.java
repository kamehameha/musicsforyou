package com.example.music.offline;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.music.R;
import com.example.music.model.Song;

import java.util.ArrayList;

public class CustomGridAlbumAdapter extends BaseAdapter {

    private ArrayList<Song> listOfflineAlbums;
    private LayoutInflater layoutInflater;
    private Context context;

    private MyUtil myUtil;

    public CustomGridAlbumAdapter(Context aContext, ArrayList<Song> listOfflineAlbums){
        this.context=aContext;
        this.listOfflineAlbums=listOfflineAlbums;
        layoutInflater=LayoutInflater.from(aContext);

        myUtil= new MyUtil(context);
    }



    @Override
    public int getCount() {
        return listOfflineAlbums.size();
    }

    @Override
    public Object getItem(int i) {
        return listOfflineAlbums.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_gridview_album, null);
            holder = new ViewHolder();
            holder.flagView = (ImageView) view.findViewById(R.id.item_grid_imgAlbum);
            holder.tvAlbumName= (TextView) view.findViewById(R.id.item_grid_tvAlbumName);
            holder.tvArtist = view.findViewById(R.id.item_grid_tvArtist);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Song song = this.listOfflineAlbums.get(i);
        holder.tvAlbumName.setText(song.getAlbum());
        holder.tvArtist.setText(song.getSinger());
        Bitmap bm;
        String name = song.getSongname() + song.getSinger() +song.getAlbum();//.substring(0, .lastIndexOf('.'))
        bm= myUtil.loadImageBitmap(name); // myUtil.getAlbumArt(audio.getData());
        if(bm == null) {
            bm = myUtil.getAlbumArt(song.getLinkmp3());
            if (bm == null)
                holder.flagView.setImageResource(R.drawable.defaultimage);
            //else myUtil.saveImage(bm,name);
        }
            else holder.flagView.setImageBitmap(bm);


        return view;
    }

    static class ViewHolder {
        ImageView flagView;
        TextView tvAlbumName;
        TextView tvArtist;
    }
}
