package com.example.music;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.example.music.model.AudioPlayService;
import com.example.music.model.Song;

import com.example.music.model.User;
import com.example.music.offline.MyUtil;
import com.example.music.offline.OfflineFrag;
import com.example.music.offline.OfflineFragTabAlbum;
import com.example.music.offline.OfflineFragTabPlaylist;
import com.example.music.offline.OfflinePlayingActivity;
import com.example.music.ui.testfragment.homefrag;
import com.example.music.ui.testfragment.threefrag;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
 //    http://192.168.0.4:8080/
//    http://goodmooduit.000webhostapp.com/
//    207.148.73.115
    public static final String HOST = "http://207.148.73.115/";
    private static MediaPlayer MEDIAPLAYER ;
    public static String MEDIAPLAYER_url ;

    public static final String STORAGE = "musicapi/storage/";

    private static final int REQUEST_CODE_READ_EXTERNAL_PERMISSION = 2; // Load song offline

//    private static MediaPlayer currentMEDIAPLAYER;
//    public static String currentMEDIAPLAYER_url;
//    private static MediaPlayer nextMEDIAPLAYER;
//    public static String nextMEDIAPLAYER_url;



//    public static String IDUSER  = "1";

    public static User USER;
//    public static SystemMusic SYSTEMMUSIC;
    private Button btnSearch;
    CircularImageView imgAvatarUserHome;


    private Intent foregroundAudioIntent;

    public static AudioPlayService mAudioPlayService;

    private ArrayList<Song> loadAudio() {

        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);
        //audioList = new ArrayList<>();
        MyUtil myUtil = new MyUtil(getApplicationContext());
        boolean isFirstSaveAlbumImages = myUtil.loadStateSaveAlbumImages();
        ArrayList<Song> audioList = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            //MyUtil myutil = new MyUtil(getApplicationContext());

            int index = 0;
            Bitmap bm;
            while (cursor.moveToNext()) {

                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
               // String composer = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.COMPOSER));
                String albumId = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));

                // Save to audioList
                if(!data.endsWith("flac") && !data.endsWith("FLAC")){
                    audioList.add(new Song(data, title, album, artist,duration, albumId));
                }

                if(isFirstSaveAlbumImages){
                    bm = myUtil.getAlbumArt(data);
                    if(bm!=null)
                        bm =Bitmap.createScaledBitmap(bm, 200, 200, false);
                    myUtil.saveImage(bm,title+artist+album);
                }
            }
            myUtil.storeStateSaveAlbumImages(false);
        }
        cursor.close();
        dialog.cancel();
        Toast.makeText(this,"OK Good Mood!!!",Toast.LENGTH_SHORT).show();

        return audioList;
    }

    private void loadSongOffline(){
        if(OfflineFragTabPlaylist.mListSong ==null){
            //Toast.makeText(getContext(),"mListSong == null",Toast.LENGTH_SHORT).show();
            OfflineFragTabPlaylist.mListSong = new ArrayList<>();
            int readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(this, requirePermission, REQUEST_CODE_READ_EXTERNAL_PERMISSION);
                //mListSong = loadAudio();
            } else {
                //Toast.makeText(getContext(),"mListSong != null",Toast.LENGTH_SHORT).show();
                OfflineFragTabPlaylist.mListSong = loadAudio();
            }
//            if(mListSong!=null && mListSong.size()>0)
//            mService.playSongsOffline(mListSong,0);
//            else    Toast.makeText(getContext(),"myListAudio: null",Toast.LENGTH_SHORT).show();
        }
    }

    private void loadAlbumOffline(){
        if(OfflineFragTabAlbum.listAlbumDistinctByName ==null)
            OfflineFragTabAlbum.listAlbumDistinctByName = OfflineFragTabAlbum.makeListAlbumDistinctByName();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_READ_EXTERNAL_PERMISSION)
        {
            if(grantResults.length > 0)
            {
                int grantResult = grantResults[0];
                if(grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    // If user grant the permission then open browser let user select audio file.
                    //selectAudioFile();
                    Toast.makeText(getApplicationContext(), "Start loading audio, Please wait along time!", Toast.LENGTH_LONG).show();
                   OfflineFragTabPlaylist.mListSong =  loadAudio();
                }else
                {
                    Toast.makeText(getApplicationContext(), "You denied read external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //init Fast Andr Networking
        AndroidNetworking.initialize(getApplicationContext());

        dialog =new ProgressDialog(this);
        dialog.setMessage("Đang đọc bài hát từ máy, đợi chút nha...!");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.show();

        final MyUtil myUtil = new MyUtil(this);

        //OfflineFragTabPlaylist.mListSong = myUtil.loadListSong();
        //if(OfflineFragTabPlaylist.mListSong== null){
            //Log.i("mListSong","mListSong == null");
        loadSongOffline();
            //myUtil.storeListSong(OfflineFragTabPlaylist.mListSong);
        loadAlbumOffline();



        storePosition =  MyUtil.loadPosition();

        if(myUtil.loadStateSaveAlbumImages()==false)
            dialog.cancel();


        btnSearch=findViewById(R.id.main_btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);

                startActivity(intent);
            }
        });

        //load recent login
        //final MyUtil myUtil = new MyUtil(getApplicationContext());
        //
//        Audio audio ;//= audioList.get(15);
//        Bitmap bm ;//= myUtil.loadImageBitmap(String.valueOf(audio.getIndexSortedByTitle()),"png");
////
////        if(bm==null){
//            for(int i = 0;i<audioList.size();i++){
//                audio = audioList.get(i);
//                bm = myUtil.getAlbumArt(audio.getData());
//                String name = audio.getTitle() + audio.getArtist() + audio.getAlbumID();
//                myUtil.saveImage(bm,name);
//            }
        //}
        //
        User recentAudio = myUtil.loadUser();
        if(recentAudio==null){
            MainActivity.USER = MainActivity.createUserNormal();
        }
        else {
            MainActivity.USER = recentAudio;
        }
//        if (MainActivity.USER == null) {
//            MainActivity.USER = MainActivity.createUserNormal();
//        }

        imgAvatarUserHome=findViewById(R.id.imgAvatarUserHome);

//        imgAvatarUserHome.setBorderWidth(1);
//        imgAvatarUserHome.setBorderColor(Color.RED);
//        Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg()).into(imgAvatarUserHome);
        reloadUser();

        imgAvatarUserHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.USER.getUsername().compareTo("Anonymous") == 0 && MainActivity.USER.getIduser().compareTo("1") == 0 ){
                    Intent intent = new Intent(getApplicationContext(), login.class);

                    startActivityForResult(intent, 1);
//                    startActivity(intent);
                }else{


                    PopupMenu popup = new PopupMenu(MainActivity.this, imgAvatarUserHome);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_useravartar, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId())   {

                                case R.id.menuitem_userfunc:
                                    Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                                    startActivity(intent);
                                    break;

                                case R.id.menuitem_logout:
                                    myUtil.storeUser(null);
                                    MainActivity.USER = MainActivity.createUserNormal();
                                    //reload user main activ
                                    reloadUser();
                                    break;


                                default:
                                    break;
                            }

//                            Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            }
        });


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rl_activmain_1);

        AnimationDrawable animationDrawable = (AnimationDrawable) relativeLayout.getBackground();

        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);

        animationDrawable.start();
//        USER=createUserNull();
//        if (USER == null) {
//            USER = createUserNormal();
//        }





        if (Util.SDK_INT >= 24) {
            foregroundAudioIntent = new Intent(this,AudioPlayService.class);
        }
        else {
            throw new UnsupportedOperationException("Not yet implemented");
        }


        bindService(foregroundAudioIntent, mConnection, BIND_AUTO_CREATE);
        Util.startForegroundService(this, foregroundAudioIntent);



        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.containerfrag, new homefrag()).commit();


        //loadPlayerControlView();

        if(mAudioPlayService!=null){

//            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
//                    new IntentFilter("playingfrag_playnewsong"));
            mService=mAudioPlayService;
            loadPlayerControlView();
            loadNewSongInfomation();

        }

        Log.i("huychuong","onCreate");

    }

    private void reloadUser() {
        if (USER != null) {
            imgAvatarUserHome.setBorderWidth(1);
            imgAvatarUserHome.setBorderColor(Color.RED);
            Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg()).into(imgAvatarUserHome);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_useravartar, menu);
        return true;
    }

    private void loadPlayerControlView(){

        //Huychuong add this

        //mService = MainActivity.mAudioPlayService;
        playerControlView=this.findViewById(R.id.main_player_control_view);

        tvSongNameOnPanel = this.findViewById(R.id.main_songTitle_panel_control_view);
        tvSongArtistOnPanel = this.findViewById(R.id.main_songArtist_panel_control_view);
        tvSongNameOnPanel.setSelected(true);
        tvSongArtistOnPanel.setSelected(true);

        mImageThumbAlbumArt= this.findViewById(R.id.main_thumb_AlbumArt_panel_control_view);
        mImageThumbAlbumArt.setFocusedByDefault(false);
        mImageThumbAlbumArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                turnOnMainPlaying();
            }
        });

        final LinearLayout myLinear =  this.findViewById(R.id.main_linear_panel_control_view);
        myLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                turnOnMainPlaying();
            }
        });

        //loadNewSongInfomation();
    }

    private  BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            //Log.d("sendMessageToPlayingFrag_receiver", "Got message: " + message);
            Log.i("onbroadcast", "received");
            loadNewSongInfomation();
        }
    };

    private void initializePlayer() {
        if (player == null ) {
            player = MainActivity.mAudioPlayService.getPlayer();
            //player = mService.getPlayer();

            playerControlView.setPlayer(player);

            loadLastedSong();


        }
    }

    private void releasePlayer() {
        if (player != null) {
//            playWhenReady = player.getPlayWhenReady();
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();

//            player.removeListener(playbackStateListener);

//            player.release();
            player = null;
            playerControlView.setPlayer(null);
        }
    }

    private void turnOnMainPlaying(){
        if(mService.getPlayingList()==null)
            return;
        Intent intent;
        if(mService.isPlayingOnline())
            intent = new Intent(this, OnlineExoPlayerActivity.class);
        else
            intent = new Intent(this, OfflinePlayingActivity.class);
        //getActivity().finish();
        startActivity(intent);
    }

    private AudioPlayService mService;
    private SimpleExoPlayer player;
    private PlayerControlView playerControlView;
    private TextView tvSongNameOnPanel;
    private TextView tvSongArtistOnPanel;

    private ImageView mImageThumbAlbumArt;

    private void loadNewSongInfomation(){

        if( mService.getPlayingList()==null )
            return;
        //Toast.makeText(this,"loadNewSongInfo",Toast.LENGTH_SHORT).show();

        Song song = mService.getPlayingSong();
        if(mService.isPlayingOnline()){
            Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + mService.getPlayingSong().getImg()).into(mImageThumbAlbumArt);
        }
        else {
            MyUtil myutil = new MyUtil(getApplicationContext());
            Bitmap bm = myutil.getAlbumArt(song.getLinkmp3());
            if (bm == null)
                bm = BitmapFactory.decodeResource(getResources(), R.drawable.defaultimage);
            mImageThumbAlbumArt.setImageBitmap(bm);
        }
        tvSongNameOnPanel.setText(song.getSongname());
        tvSongArtistOnPanel.setText(song.getSinger());

        MyUtil.rotateImage(mImageThumbAlbumArt,3000);

        Log.i("loadsonginfo", "Loaded...");


    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mAudioPlayService = ((AudioPlayService.LocalBinder) iBinder).getServerInstance();
            // now you have the instance of service.
            Log.i("huychuong","onServiecConnected");

            mService=mAudioPlayService;

            loadPlayerControlView();
            initializePlayer();

            loadNewSongInfomation();

            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                    new IntentFilter("playingfrag_playnewsong"));


        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mAudioPlayService = null;
            //Log.i("huychuong","onServiecDisconnected");
        }
    };

    private boolean isServiceConnected = false;

    @Override
    protected void onStart() {
        super.onStart();
        //Log.i("huychuong","onStart");
        if (Util.SDK_INT >= 24) {
            //if(isServiceConnected)
                //initializePlayer();
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (Util.SDK_INT < 24) {
//            releasePlayer();
//        }
//    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player!=null)
            MyUtil.storePosition((int)player.getCurrentPosition());
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (Util.SDK_INT >= 24) {
            //initializePlayer();
        }
    }

    @Override
    protected void onDestroy() {
        //Log.i("huychuong","onDestroy");
        if (mAudioPlayService != null) {
            // Detach the service connection.
            unbindService(mConnection);
        }

        releasePlayer();
        //playerControlView.setPlayer(null);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);


        super.onDestroy();
    }

    @NotNull
    public static User createUserNormal() {
        return new User("1", "Anonymous", "img/4.jpg", "normal", "2019-10-02 22:06:53");
    }
    public static User createUserNull() {
        return new User("", "", "", "", "");
    }

    //OfflineFrag offlineFrag = new OfflineFrag();
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_1:
                            selectedFragment = new homefrag();
                            break;
                        case R.id.nav_2:
                            selectedFragment =new OfflineFrag(); //favoritesfrag();
                            break;
                        case R.id.nav_3:
                            selectedFragment = new threefrag();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.containerfrag,selectedFragment).commit();

                    return true;
                }
            };



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==1)
//        {
//            String message=data.getStringExtra("result");
//            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
//
//        }
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");

//                Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg()).into(imgAvatarUserHome);
                reloadUser();
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }

        }
    }//onActivityResult
    private int storePosition;
    private void loadLastedSong(){
        Song song = MyUtil.loadAudio();
        if(song!=null && mService.getPlayingList()==null) {
            ArrayList<Song> lastedSong = new ArrayList<>();
            lastedSong.add(song);

            if(song.getDuration()>0)
                mService.playSongsOffline(lastedSong,0);
            else
                mService.playSongsOnline(lastedSong,0);

            mService.getPlayer().setPlayWhenReady(false);

            //storePosition = MyUtil.loadPosition();

            if(song.getDuration()>(long)storePosition)
                player.seekTo((long)storePosition);
            Log.i("position",song.getDuration() + " | " + storePosition);

        }
    }

}
