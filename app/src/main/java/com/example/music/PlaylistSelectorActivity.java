package com.example.music;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.music.model.Playlist;
import com.example.music.model.Song;
import com.example.music.ui.MyPlayListViewAdapter;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlaylistSelectorActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    String idsong ;
    String songname ;
    String singer;
    String img;

    Song song;
    private ArrayList<Playlist> listPlaylist;
    private MyListView listviewPlaylist;
    MyPlayListViewAdapter playlistAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_selector);


//        if (savedInstanceState == null) {
//            Bundle extras = getIntent().getExtras();
////            if(extras == null) {
////                idsong= null;
////                songname= null;
////                idsong= null;
//            song= null;
////            } else {
////                idsong= extras.getString("idsong");
////                songname= extras.getString("songname");
////                idsong= extras.getString("idsong");
////                song= extras.getString("song");
////            }
//        } else {
////            idsong= (String) savedInstanceState.getSerializable("idsong");
////            songname= (String) savedInstanceState.getSerializable("songname");
////            idsong= (String) savedInstanceState.getSerializable("idsong");
//            song = (Song) savedInstanceState.getSerializable("song");
//        }

        Intent i = getIntent();
        song = (Song) i.getSerializableExtra("song");

        if (song == null){
            Toast.makeText(getApplicationContext(), "Đầu vào không có giá trị Song", Toast.LENGTH_SHORT).show();
        }


         idsong = song.getIdsong();
         songname = song.getSongname();
         singer = song.getSinger();
         img = song.getImg();

         TextView tvSongName = findViewById(R.id.tv_playlist_selector_songname);
         TextView tvSinger = findViewById(R.id.tv_playlist_selector_singer);

        tvSongName.setText(songname);
        tvSinger.setText(singer);

         CircularImageView imgAvar = findViewById(R.id.civ_playlist_selector_img);
        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + img ).into(imgAvar);
        imgAvar.setBorderColor(Color.BLACK);
        imgAvar.setBorderWidth(3);
        imgAvar.setCircleColor(Color.WHITE);
        imgAvar.setShadowRadius(3);
        imgAvar.setShadowColor(Color.BLUE);
//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);



        listPlaylist = new ArrayList<>();
        listviewPlaylist = findViewById(R.id.mlv_playlist_selector_1);

        playlistAdapter = new MyPlayListViewAdapter(this, R.layout.row_mylistview, listPlaylist);

        listviewPlaylist.setAdapter(playlistAdapter);

        getPlaylistsOfUser();

        listviewPlaylist.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Playlist pl = (Playlist) adapterView.getItemAtPosition(position);

        String idpl = pl.getIdpl();
        String idsong = song.getIdsong();
        String iduser = MainActivity.USER.getIduser();

        //add song into playlist
        addSongIntoPlaylist(idpl, idsong, iduser);
    }



    private void getPlaylistsOfUser() {

        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getPlaylistOfUser")
                .addBodyParameter("iduser", MainActivity.USER.getIduser())
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONArray ja = response.getJSONArray("response");

                                String idpl;
                                String plname ;
                                String img;
                                String type;
                                String rating_ ;
                                String plstatus ;
                                String shared ;
                                String iduser ;
                                String created_at ;

                                Playlist pl;

                                for (int i=0; i < ja.length(); i++) {
                                    idpl = ja.getJSONObject(i).getString("idpl");
                                    plname = ja.getJSONObject(i).getString("plname");
                                    img = ja.getJSONObject(i).getString("img");
                                    type = ja.getJSONObject(i).getString("type");
                                    rating_ = ja.getJSONObject(i).getString("rating_");
                                    plstatus = ja.getJSONObject(i).getString("status");
                                    shared = ja.getJSONObject(i).getString("shared");
                                    iduser = ja.getJSONObject(i).getString("iduser");
                                    created_at = ja.getJSONObject(i).getString("created_at");


                                    pl = new Playlist(idpl, plname, img,
                                            type, rating_, plstatus, shared,iduser,
                                            created_at);

                                    listPlaylist.add(pl);

                                }
                                playlistAdapter.notifyDataSetChanged();

                            }
                            else {

                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }



    private void addSongIntoPlaylist(String idpl, String idsong, String iduser) {
        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/songs/addsongintoplaylist")
                .addBodyParameter("iduser", iduser)
                .addBodyParameter("idsong", idsong)
                .addBodyParameter("idpl", idpl)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
}
