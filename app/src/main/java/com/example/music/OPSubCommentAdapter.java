package com.example.music;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OPSubCommentAdapter extends ArrayAdapter<OPComment> {
    private Context context;
    private int resource;
    private List<OPComment> arrOPComment;

    public OPSubCommentAdapter(@NonNull Context context, int resource, @NonNull ArrayList<OPComment> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.arrOPComment = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        }


//        View row = inflater.inflate(this.resource,null);

        TextView tvUser = (TextView) convertView.findViewById(R.id.activsubcomment_tvUser);
        TextView tvContent = (TextView) convertView.findViewById(R.id.activsubcomment_tvContent);
        TextView tvTime = (TextView) convertView.findViewById(R.id.activsubcomment_tvCommentAtTime);

        CircularImageView imgAvatar = (CircularImageView) convertView.findViewById(R.id.activsubcomment_imgAvatar);

//        ListView lv_subcomments = findViewById(R.id.lv_op_subcomment);
//
//        ArrayList<OPComment> listsubcomment = new ArrayList<OPComment>();
//
//
//        songAdapter = new OPCommentAdapter(this, R.layout.row_lv_subcomment_online_player, listcomment);
//
//        listView_comments.setAdapter(songAdapter);

        /** Set data to row*/
        final OPComment book = this.arrOPComment.get(position);
        tvUser.setText(book.getUser());
        tvContent.setText(book.getContent());
        tvTime.setText(book.getAt_time());
//        imgAvatar.setText(book.getAvatar());

        /**Set Event Onclick*/
//        btnLike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showMessage(book);
//            }
//        });

        Picasso.get().load(MainActivity.HOST + MainActivity.STORAGE + book.getAvatar()).into(imgAvatar);
        imgAvatar.setBorderColor(Color.BLACK);
        imgAvatar.setBorderWidth(3);
        imgAvatar.setCircleColor(Color.WHITE);
        imgAvatar.setShadowRadius(3);
        imgAvatar.setShadowColor(Color.BLUE);
//        imgAvatar.setBackgroundColor(Color.RED);
        imgAvatar.setShadowGravity(CircularImageView.ShadowGravity.CENTER);

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //lam j do o day
//            }
//        });
        return convertView;

//        return super.getView(position, convertView, parent);
    }
}
