package com.example.music;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.example.music.model.Playlist;
import com.example.music.ui.MyPlayListViewAdapter;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
//    private ListView lv_userplaylist;
    private ArrayList<Playlist> listPlaylist;
//    ArrayAdapter lv_userplaylist_adapter;

    private ImageButton imgbtnUser1;
    private TextView txtViewUser1;
    private CircularImageView imgViewUser1;
    private EditText etAddPl;
    private Button btnAddPl;


//    private ArrayList<MyListViewDataset> listcomment;
    private ListView listviewPlaylist;
    MyPlayListViewAdapter playlistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        imgbtnUser1 = findViewById(R.id.imgbtnUser1);
//        lv_userplaylist = findViewById(R.id.lv_playlist_useractiv);
        imgViewUser1 = findViewById(R.id.imgViewUser1);
        txtViewUser1 = findViewById(R.id.txtViewUser1);

        listPlaylist = new ArrayList<>();
//        listPlaylist.add(new Playlist("10","hay trao cho anh 1","img/1.jpg","playlist",
//                "10","1","1","1","",null));
//        listPlaylist.add(new Playlist("10","hay trao cho anh 2","img/1.jpg","playlist",
//                "10","1","1","1","",null));
//        listPlaylist.add(new Playlist("10","hay trao cho anh 3","img/1.jpg","playlist",
//                "10","1","1","1","",null));
        //nap du lieu vao listview


//         lv_userplaylist_adapter  = new ArrayAdapter(this,
//                android.R.layout.simple_list_item_1, listPlaylist);
//        lv_userplaylist.setAdapter(lv_userplaylist_adapter);


        listviewPlaylist = findViewById(R.id.lv_playlist_useractiv);

//        listcomment = new ArrayList<>();

        playlistAdapter = new MyPlayListViewAdapter(this, R.layout.row_myplaylist_view, listPlaylist);

        listviewPlaylist.setAdapter(playlistAdapter);

        getPlaylistsOfUser();

        txtViewUser1.setText(MainActivity.USER.getUsername());
//        Picasso.get().load(MainActivity.HOST + "musicapi/storage/" + MainActivity.USER.getImg()).into(imgViewUser1);





// Reference to an image file in Cloud Storage
//        StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(MainActivity.FIREBASEHOST + "musicapi/storage/" + MainActivity.USER.getImg());

// ImageView in your Activity
//        ImageView imageView = imgViewUser1;
//        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                uri.toString();
//
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//
//            }
//        });
// Download directly from StorageReference using Glide
// (See MyAppGlideModule for Loader registration)

        Glide.with(this /* context */)
                .load(MainActivity.HOST + MainActivity.STORAGE + MainActivity.USER.getImg())
                .into(imgViewUser1);

        imgbtnUser1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "add new playlist - test player", Toast.LENGTH_SHORT).show();
                DialogAddNewPl();
//                Intent intent = new Intent(getApplicationContext(), testplayerActivity.class);
//
//                startActivity(intent);
            }
        });

        listviewPlaylist.setOnItemClickListener(this);

//        listviewSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Playlist pl = (Playlist) adapterView.getItemAtPosition(position);
//
//                String idpl = pl.getIdpl();
//
//                Log.i("MyListViewAdapter", "Playlist onItemClick: onClick" +idpl );
//
//                Intent intent = new Intent(getApplicationContext(), PlaylistActivity.class);
//                intent.putExtra("idpl", idpl);
//
//                Toast.makeText(getApplicationContext(), pl.toString(), Toast.LENGTH_SHORT).show();
//
//                startActivity(intent);
//            }
//        });

    }


    private void getPlaylistsOfUser() {
        AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/getPlaylistOfUser")
                .addBodyParameter("iduser", MainActivity.USER.getIduser())
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Boolean status = response.getBoolean("status");
                            String message = response.getString("message");

                            if (status) {
                                JSONArray ja = response.getJSONArray("response");

                                String idpl;
                                String plname ;
                                String img;
                                String type;
                                String rating_ ;
                                String plstatus ;
                                String shared ;
                                String iduser ;
                                String created_at ;

                                Playlist pl;

                                for (int i=0; i < ja.length(); i++) {
                                    idpl = ja.getJSONObject(i).getString("idpl");
                                    plname = ja.getJSONObject(i).getString("plname");
                                    img = ja.getJSONObject(i).getString("img");
                                    type = ja.getJSONObject(i).getString("type");
                                    rating_ = ja.getJSONObject(i).getString("rating_");
                                    plstatus = ja.getJSONObject(i).getString("status");
                                    shared = ja.getJSONObject(i).getString("shared");
                                    iduser = ja.getJSONObject(i).getString("iduser");
                                    created_at = ja.getJSONObject(i).getString("created_at");


                                    pl = new Playlist(idpl, plname, img,
                                            type, rating_, plstatus, shared,iduser,
                                            created_at);

                                    listPlaylist.add(pl);

                                }
                                playlistAdapter.notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Log.i("MyListViewAdapter", "Playlist onItemClick: onClick" + position );
        // listPlaylist.get(position).getIdpl();
        Playlist pl = (Playlist) adapterView.getItemAtPosition(position);

        String idpl = pl.getIdpl();
        Intent intent = new Intent(getApplicationContext(), PlaylistActivity.class);
        intent.putExtra("idpl", idpl);

        Toast.makeText(getApplicationContext(), pl.toString(), Toast.LENGTH_SHORT).show();

        startActivity(intent);

        ////////////


    }
    private void DialogAddNewPl(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_addnewplaylist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        etAddPl=(EditText) dialog.findViewById(R.id.EText4);
        btnAddPl=(Button) dialog.findViewById(R.id.btnAddPl);
        btnAddPl.setEnabled(false);

        etAddPl.addTextChangedListener(new TextWatcher() {//Neu ki tu them <0 thi button.enable=false

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Test",String.valueOf(charSequence.length()));
                if(charSequence.length()>0) {btnAddPl.setBackgroundResource(R.color.color8);btnAddPl.setEnabled(true);}
                if(charSequence.length()==0) {btnAddPl.setEnabled(false);}
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnAddPl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AndroidNetworking.post(MainActivity.HOST + "musicapi/index.php/api/playlists/insertPlaylist")
                        .addBodyParameter("iduser", MainActivity.USER.getIduser())
                        .addBodyParameter("plname", etAddPl.getText().toString())
                        .setTag("test")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {

                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                Boolean status = null;
                                try {
                                    status = response.getBoolean("status");

                                String message = response.getString("message");

                                if (status) {

                                    listPlaylist.clear();
                                    getPlaylistsOfUser();

                                }
                                else {

                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });
                dialog.dismiss();
            };

        });

    }

}

